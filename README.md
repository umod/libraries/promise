# Promise

Designed to match JS (es6+) Promises but with a few differences to allow for better performance.

This project originally started as a fork of [Real-Serious-Games/C-Sharp-Promise](https://github.com/Real-Serious-Games/C-Sharp-Promise/) but it has largely diverged from it.
