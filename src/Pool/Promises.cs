﻿namespace uMod
{
    internal static class Promises<TValue>
    {
        public static readonly PromisePool<Promise<TValue>> Pool = new PromisePool<Promise<TValue>>();
    }
}
