﻿namespace uMod
{
    public class PromiseTypeException : PromiseException
    {
        internal PromiseTypeException() : base("Trying to resolve promise with incorrect type")
        {
        }
    }
}
