﻿using System;

namespace uMod
{
    public class PromiseStateException : PromiseException
    {
        internal PromiseStateException()
        {
        }

        internal PromiseStateException(string message) : base(message)
        {
        }

        internal PromiseStateException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
