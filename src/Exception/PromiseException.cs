﻿using System;

namespace uMod
{
    public class PromiseException : Exception
    {
        internal PromiseException()
        {
        }

        internal PromiseException(string message) : base(message)
        {
        }

        internal PromiseException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
