﻿using System;

namespace uMod
{
    public class PromiseUnhandledException : PromiseException
    {
        internal PromiseUnhandledException(Exception innerException) : base("An unhandled exception was thrown", innerException)
        {
        }
    }
}
