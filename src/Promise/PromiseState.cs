﻿namespace uMod
{
    internal enum PromiseState
    {
        Pending,
        Rejected,
        Fulfilled
    }
}
