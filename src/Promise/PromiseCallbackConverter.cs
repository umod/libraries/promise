﻿using System;

namespace uMod
{
    internal sealed class PromiseCallbackConverter
    {
        public static class Generic
        {
            public static class Fulfilled
            {
                public static PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? Callback<TValue, TConvertedValue>(PromiseOnFulfillFunc<IPromise<TConvertedValue?>?>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => onFulfilled();
                }

                public static PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? Callback<TValue, TConvertedValue>(PromiseOnFulfillFunc<TConvertedValue>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => Promise.Resolve<TConvertedValue?>(onFulfilled());
                }

                public static PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? Callback<TValue, TConvertedValue>(PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => onFulfilled(value);
                }

                public static PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? Callback<TValue, TConvertedValue>(PromiseOnFulfillFunc<TValue, TConvertedValue>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => Promise.Resolve<TConvertedValue?>(onFulfilled(value));
                }

                public static PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? Callback<TValue, TConvertedValue>(PromiseOnSettled<TConvertedValue>? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (TValue value) =>
                    {
                        onSettled();
                        return Promise.Resolve<TConvertedValue>();
                    };
                }

                public static PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? Callback<TValue, TConvertedValue>(PromiseOnSettled<IPromise<TConvertedValue?>?> onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (TValue value) => onSettled();
                }
            }

            public static class Rejected
            {
                public static PromiseOnReject<IPromise<TConvertedValue?>?>? Callback<TConvertedValue>(PromiseOnReject<TConvertedValue>? onRejected)
                {
                    if (onRejected == null)
                        return null;

                    return (Exception reason) => Promise.Resolve<TConvertedValue?>(onRejected(reason));
                }

                public static PromiseOnReject<IPromise<TConvertedValue?>?>? Callback<TConvertedValue>(PromiseOnSettled<TConvertedValue>? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (Exception reason) =>
                    {
                        onSettled();
                        return Promise.Resolve<TConvertedValue>();
                    };
                }

                public static PromiseOnReject<IPromise<TConvertedValue?>?>? Callback<TConvertedValue>(PromiseOnSettled<IPromise<TConvertedValue?>?>? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (Exception reason) => onSettled();
                }
            }
        }

        public static class NonGeneric
        {
            public static class Fulfilled
            {
                public static PromiseOnFulfillFunc<TValue, IPromise?>? Callback<TValue>(PromiseOnFulfillAction? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) =>
                    {
                        onFulfilled();
                        return Promise.Resolve();
                    };
                }

                public static PromiseOnFulfillFunc<TValue, IPromise?>? Callback<TValue>(PromiseOnFulfillAction<TValue>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) =>
                    {
                        onFulfilled(value);
                        return Promise.Resolve();
                    };
                }

                public static PromiseOnFulfillFunc<TValue, IPromise?>? Callback<TValue>(PromiseOnFulfillFunc<IPromise?>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => onFulfilled();
                }

                public static PromiseOnFulfillFunc<TValue, IPromise?>? Callback<TValue>(PromiseOnFulfillFunc<TValue, IPromise?>? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => onFulfilled(value);
                }

                public static PromiseOnFulfillFunc<TValue, IPromise?>? Callback<TValue>(PromiseOnSettled? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (TValue value) =>
                    {
                        onSettled();
                        return Promise.Resolve();
                    };
                }

                public static PromiseOnFulfillFunc<TValue, IPromise?>? Callback<TValue>(PromiseOnSettled<IPromise?> onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (TValue value) => onSettled();
                }
            }

            public static class Rejected
            {
                public static PromiseOnReject<IPromise?>? Callback(PromiseOnReject? onRejected)
                {
                    if (onRejected == null)
                        return null;

                    return (Exception reason) =>
                    {
                        onRejected(reason);
                        return Promise.Resolve();
                    };
                }
                public static PromiseOnReject<IPromise?>? Callback(PromiseOnSettled? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (Exception reason) =>
                    {
                        onSettled();
                        return Promise.Resolve();
                    };
                }

                public static PromiseOnReject<IPromise?>? Callback(PromiseOnSettled<IPromise?>? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (Exception reason) => onSettled();
                }
            }

            public static class Leaf
            {
                public static PromiseOnFulfillAction<TValue>? FulfilledCallback<TValue>(PromiseOnFulfillAction? onFulfilled)
                {
                    if (onFulfilled == null)
                        return null;

                    return (TValue value) => onFulfilled();
                }

                public static PromiseOnFulfillAction<TValue>? FulfilledCallback<TValue>(PromiseOnSettled? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (TValue value) => onSettled();
                }

                public static PromiseOnReject? RejectedCallback(PromiseOnSettled? onSettled)
                {
                    if (onSettled == null)
                        return null;

                    return (Exception reason) => onSettled();
                }
            }
        }
    }
}
