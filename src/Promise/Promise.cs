﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace uMod
{
    public sealed class Promise<TValue> : ISettleablePromise<TValue>, IPromiseInternals
    {
        private TValue? ResolvedValue;
        object? IPromiseInternals.ResolvedValue => ResolvedValue;

        private Exception? RejectionReason;
        Exception? IPromiseInternals.RejectionReason => RejectionReason;

        private PromiseState State;
        PromiseState IPromiseInternals.State => State;

        private bool FinishedWith;
        bool IPromiseInternals.FinishedWith { get => FinishedWith; set => FinishedWith = value; }

        private bool ExecutingCallbacks;
        bool IPromiseInternals.ExecutingCallbacks => ExecutingCallbacks;

        private bool Disposed;
        private bool Synced;

        private IList<IPromiseCallbackHandler<TValue>>? FulfillCallbacks;
        private IList<IPromiseCallbackHandler<float>>? ProgressCallbacks;
        private IList<IPromiseCallbackHandler<Exception>>? RejectionCallbacks;

        private IPromisePoolable? _ParentInChain;
        IPromisePoolable IPromisePoolable.ParentInChain { set => ParentInChain = value; }

        private IPromisePoolable? ParentInChain
        {
            get
            {
                return _ParentInChain;
            }
            set
            {
                Debug.Assert(_ParentInChain == null);
                Debug.Assert(value != null);
                _ParentInChain = value;
                value!.AddChild(this);
            }
        }

        private Dictionary<IPromisePoolable, bool>? ChildrenInChainFinished;

        private IPromiseInternals? InternalNode;
        private EventWaitHandle? EventWait;

        /// <summary>
        /// Invoked by the Activator in the Promise Pool to create new promises when needed.
        /// Nothing else should ever make new promises.
        /// </summary>
        internal Promise()
        {
        }

        void IPromisePoolable.Init()
        {
            State = PromiseState.Pending;
            Synced = false;
            FinishedWith = false;
            ExecutingCallbacks = false;
            Disposed = false;
        }

        void IPromisePoolable.Dispose()
            => Dispose();

        private void Dispose()
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("Promise");
            }

            Disposed = true;

            RejectionReason = null;
            RejectionCallbacks?.Clear();
            FulfillCallbacks?.Clear();
            ProgressCallbacks?.Clear();
            EventWait?.Close();
            RejectionCallbacks = null;
            FulfillCallbacks = null;
            ProgressCallbacks = null;
            EventWait = null;

            if (InternalNode != null)
            {
                if (InternalNode.ExecutingCallbacks)
                {
                    InternalNode.FinishedWith = true;
                }
                else
                {
                    InternalNode.Dispose();
                }
            }
            InternalNode = null;

            if (ParentInChain != null)
            {
                ParentInChain.ReportChildFinished(this);
            }
            _ParentInChain = null;
            ChildrenInChainFinished?.Clear();
            ChildrenInChainFinished = null;

            Promises<TValue>.Pool.Free(this);
        }

        private void CleanUp()
        {
            RejectionCallbacks = null;
            FulfillCallbacks = null;
            ProgressCallbacks = null;

            if (FinishedWith)
            {
                Dispose();
            }
        }

        /// <summary>
        /// Appends a progress handler callback to the promise.
        /// </summary>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>The promise (this).</returns>
        IPromise IPromise.Progress(PromiseOnProgress onProgress)
            => Progress(onProgress);

        /// <summary>
        /// Appends a progress handler callback to the promise.
        /// </summary>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>The promise (this).</returns>
        public IPromise<TValue> Progress(PromiseOnProgress onProgress)
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("Promise");
            }

            if (State == PromiseState.Pending && onProgress != null)
            {
                AddOnProgressCallback(PromiseCallbackHandler.Create(this, onProgress));
            }

            return this;
        }

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillAction onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback<TValue>(onFulfilled),
                PromiseCallbackConverter.NonGeneric.Rejected.Callback(onRejected),
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillFunc<IPromise?> onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback<TValue>(onFulfilled),
                PromiseCallbackConverter.NonGeneric.Rejected.Callback(onRejected),
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillAction onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback<TValue>(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillFunc<IPromise?> onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback<TValue>(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TConvertedValue> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null)
        {
            if (typeof(IPromise).IsAssignableFrom(typeof(TConvertedValue)))
            {
                throw new PromiseException("Invalid Then overload called, cast the return value of the callback(s) to IPromise or IPromise<T> to ensure the right overload is called");
            }

            return Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback<TValue, TConvertedValue>(onFulfilled),
                PromiseCallbackConverter.Generic.Rejected.Callback(onRejected),
                onProgress
            );
        }

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TConvertedValue> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback<TValue, TConvertedValue>(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback<TValue, TConvertedValue>(onFulfilled),
                PromiseCallbackConverter.Generic.Rejected.Callback(onRejected),
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback<TValue, TConvertedValue>(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillAction<TValue> onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback(onFulfilled),
                PromiseCallbackConverter.NonGeneric.Rejected.Callback(onRejected),
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillFunc<TValue, IPromise?> onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback(onFulfilled),
                PromiseCallbackConverter.NonGeneric.Rejected.Callback(onRejected),
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillAction<TValue> onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise Then(PromiseOnFulfillFunc<TValue, IPromise?> onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke<object?>(
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, TConvertedValue> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null)
        {
            if (typeof(IPromise).IsAssignableFrom(typeof(TConvertedValue)))
            {
                throw new PromiseException("Invalid Then overload called, cast the return value of the callback(s) to IPromise or IPromise<T> to ensure the right overload is called");
            }

            return Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback(onFulfilled),
                PromiseCallbackConverter.Generic.Rejected.Callback(onRejected),
                onProgress
            );
        }

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, TConvertedValue> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback(onFulfilled),
                PromiseCallbackConverter.Generic.Rejected.Callback(onRejected),
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        public IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback(onFulfilled),
                onRejected,
                onProgress
            );

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise IPromise.ThenPeek(PromiseOnFulfillAction? onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress)
            => ThenPeek(onFulfilled, onRejected, onProgress);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        public IPromise<TValue> ThenPeek(PromiseOnFulfillAction? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null)
        {
            InvokeLeaf(PromiseCallbackConverter.NonGeneric.Leaf.FulfilledCallback<TValue>(onFulfilled), onRejected, onProgress, false);
            return this;
        }

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        public IPromise<TValue> ThenPeek(PromiseOnFulfillAction<TValue>? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null)
        {
            InvokeLeaf(onFulfilled, onRejected, onProgress, false);
            return this;
        }

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.<br/>
        /// But unlike the Then method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// <para/>
        /// After the onFulfilled or onRejected callback is invoked, the promise will be disposed of
        /// as soon as all callbacks it current has have been called and finished successfully. This
        /// will happen immediately if the promise is already settled when this method is called.<br/>
        /// Additionally, all promise before it in the promise chain will also be disposed of as well
        /// once their callbacks have completed.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        public void Done(PromiseOnFulfillAction? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null)
            => InvokeLeaf(PromiseCallbackConverter.NonGeneric.Leaf.FulfilledCallback<TValue>(onFulfilled), onRejected, onProgress, true);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.<br/>
        /// But unlike the Then method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// <para/>
        /// After the onFulfilled or onRejected callback is invoked, the promise will be disposed of
        /// as soon as all callbacks it current has have been called and finished successfully. This
        /// will happen immediately if the promise is already settled when this method is called.<br/>
        /// Additionally, all promise before it in the promise chain will also be disposed of as well
        /// once their callbacks have completed.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        public void Done(PromiseOnFulfillAction<TValue>? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null)
            => InvokeLeaf(onFulfilled, onRejected, onProgress, true);

        void IPromiseInternals.DoneNonGeneric(PromiseOnFulfillAction<object?>? onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress)
            => InvokeLeaf(
                onFulfilled == null ? null : (TValue value) => onFulfilled(value),
                onRejected,
                onProgress,
                true
            );

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        public IPromise Catch(PromiseOnReject onRejected)
            => Invoke<TValue>(null, PromiseCallbackConverter.NonGeneric.Rejected.Callback(onRejected), null);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        public IPromise Catch(PromiseOnReject<IPromise?> onRejected)
            => Invoke<TValue>(null, onRejected, null);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        public IPromise<TConvertedValue> Catch<TConvertedValue>(PromiseOnReject<IPromise<TConvertedValue?>?> onRejected)
            => Invoke(null, onRejected, null);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        public IPromise<TConvertedValue> Catch<TConvertedValue>(PromiseOnReject<TConvertedValue> onRejected)
        {
            if (typeof(IPromise).IsAssignableFrom(typeof(TConvertedValue)))
            {
                throw new PromiseException("Invalid Catch overload called, cast the return value of the callback to IPromise or IPromise<T> to ensure the right overload is called");
            }

            return Invoke(null, PromiseCallbackConverter.Generic.Rejected.Callback(onRejected), null);
        }

        /// <summary>
        /// Appends a rejection handlers to the promise but returns the same promise instead of creating
        /// a new one.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise IPromise.CatchPeek(PromiseOnReject onRejected)
            => CatchPeek(onRejected);

        /// <summary>
        /// Appends a rejection handlers to the promise but returns the same promise instead of creating
        /// a new one.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        public IPromise<TValue> CatchPeek(PromiseOnReject onRejected)
        {
            InvokeLeaf(null, onRejected, null, false);
            return this;
        }

        /// <summary>
        /// Appends a rejection handler to the promise.<br/>
        /// But unlike the Catch method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// <para/>
        /// After the onFulfilled or onRejected callback is invoked, the promise will be disposed of
        /// as soon as all callbacks it current has have been called and finished successfully. This
        /// will happen immediately if the promise is already settled when this method is called.<br/>
        /// Additionally, all promise before it in the promise chain will also be disposed of as well
        /// once their callbacks have completed.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        public void Fail(PromiseOnReject onRejected)
            => InvokeLeaf(null, onRejected, null, true);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>A new promise that is resolved when the original promise is resolved.</returns>
        public IPromise Finally(PromiseOnSettled onSettled)
            => Invoke<TValue>(
                
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback<TValue>(onSettled),
                PromiseCallbackConverter.NonGeneric.Rejected.Callback(onSettled),
                null
            );

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>A new promise that is resolved when the original promise is resolved.</returns>
        public IPromise Finally(PromiseOnSettled<IPromise?> onSettled)
            => Invoke<TValue>(
                
                PromiseCallbackConverter.NonGeneric.Fulfilled.Callback<TValue>(onSettled),
                PromiseCallbackConverter.NonGeneric.Rejected.Callback(onSettled),
                null
            );

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>A new promise that is resolved when the original promise is resolved.</returns>
        public IPromise<TConvertedValue> Finally<TConvertedValue>(PromiseOnSettled<TConvertedValue> onSettled)
        {
            if (typeof(IPromise).IsAssignableFrom(typeof(TConvertedValue)))
            {
                throw new PromiseException("Invalid Finally overload called, cast the return value of the callback to IPromise or IPromise<T> to ensure the right overload is called");
            }

            return Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback<TValue, TConvertedValue>(onSettled),
                PromiseCallbackConverter.Generic.Rejected.Callback(onSettled),
                null
            );
        }

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>A new promise that is resolved when the original promise is resolved.</returns>
        public IPromise<TConvertedValue> Finally<TConvertedValue>(PromiseOnSettled<IPromise<TConvertedValue?>?> onSettled)
            => Invoke(
                PromiseCallbackConverter.Generic.Fulfilled.Callback<TValue, TConvertedValue>(onSettled),
                PromiseCallbackConverter.Generic.Rejected.Callback(onSettled),
                null
            );

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise IPromise.FinallyPeek(PromiseOnSettled onSettled)
            => FinallyPeek(onSettled);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        public IPromise<TValue> FinallyPeek(PromiseOnSettled onSettled)
        {
            InvokeLeaf(
                PromiseCallbackConverter.NonGeneric.Leaf.FulfilledCallback<TValue>(onSettled),
                PromiseCallbackConverter.NonGeneric.Leaf.RejectedCallback(onSettled),
                null,
                false
            );
            return this;
        }

        /// <summary>
        /// Appends a settled handler to the promise.<br/>
        /// But unlike the Finally method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// <para/>
        /// After the onFulfilled or onRejected callback is invoked, the promise will be disposed of
        /// as soon as all callbacks it current has have been called and finished successfully. This
        /// will happen immediately if the promise is already settled when this method is called.<br/>
        /// Additionally, all promise before it in the promise chain will also be disposed of as well
        /// once their callbacks have completed.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        public void Complete(PromiseOnSettled onSettled)
            => InvokeLeaf((value) => onSettled(), (exception) => onSettled(), null, true);

        /// <summary>
        /// Allows for the Promise to be joined, the signal of the event is only set when the
        /// Promise reaches a conclusion of Rejected or Resolved
        /// <para/>
        /// Returns if the Promise resolved before the timeout
        /// <para/>
        /// Returns true when a negative or no timeout is given
        /// </summary>
        public bool Join(int timeout = -1)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot join a promise in state ({State}), must be ({PromiseState.Pending})");
            }
            if (Synced)
            {
                throw new PromiseStateException($"Cannot join a resolved promise");
            }

            if (EventWait == null)
            {
                EventWait = new EventWaitHandle(false, EventResetMode.ManualReset);
            }

            if (timeout < 0)
            {
                timeout = -1;
            }

            try
            {
                return EventWait.WaitOne(timeout);
            }
            catch (Exception e) when (e is ObjectDisposedException || e is AbandonedMutexException || e is InvalidOperationException)
            {
                return false;
            }
        }

        /// <summary>
        /// Resolve this promise.
        /// </summary>
        void IResolveable.ReportResolved()
            => ReportResolved();

        /// <summary>
        /// Resolve this promise.
        /// </summary>
        /// <param name="value">The value to resolve with.</param>
        public void ReportResolved(TValue? value = default)
            => ReportResolvedUnknown(value);

        /// <summary>
        /// Resolve this promise.
        /// </summary>
        /// <param name="value">A promise to match the behavior of.</param>
        public void ReportResolved(IPromise resolveWith)
        {
            if (resolveWith == null)
            {
                ReportResolvedUnknown();
                return;
            }
            Promise.Sync(resolveWith, this);
        }

        /// <summary>
        /// Resolve this promise.
        /// </summary>
        /// <param name="value">A promise to match the behavior of.</param>
        public void ReportResolved(IPromise<TValue?> resolveWith)
            => ReportResolved((IPromise)resolveWith);

        /// <summary>
        /// Resolve this promise with an unknown value.
        /// </summary>
        /// <param name="value">The value to resolve with.</param>
        private void ReportResolvedUnknown(object? value = null)
        {
            if (value is TValue tValue)
            {
                ReportFulfilled(tValue, true);
                return;
            }
            if (value is IPromise pValue)
            {
                ReportResolved(pValue);
                return;
            }
            if (value == null)
            {
                ReportFulfilled(default, true);
                return;
            }
            throw new PromiseTypeException();
        }

        private void ReportFulfilled(TValue? value, bool checkSynced)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot resolve a promise in state ({State}), must be ({PromiseState.Pending})");
            }
            if (checkSynced && Synced)
            {
                throw new PromiseStateException($"Cannot resolve an already resolved promise");
            }

            ResolvedValue = value;
            State = PromiseState.Fulfilled;
            EventWait?.Set();

            ExecutingCallbacks = true;
            InvokeProgressCallbacks(1f);
            InvokeFulfilledCallbacks(ResolvedValue);
            ExecutingCallbacks = false;

            CleanUp();
        }

        /// <summary>
        /// Reject this promise.
        /// </summary>
        /// <param name="reason">The reason for the rejection</param>
        public void ReportRejected(Exception reason)
        {
            ReportRejected(reason, true);
        }

        private void ReportRejected(Exception reason, bool checkSynced)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot reject a promise in state ({State}), must be ({PromiseState.Pending})", reason);
            }
            if (checkSynced && Synced)
            {
                throw new PromiseStateException($"Cannot reject a resolved promise");
            }

            RejectionReason = reason;
            State = PromiseState.Rejected;
            EventWait?.Set();

            ExecutingCallbacks = true;
            InvokeRejectCallbacks(reason);
            ExecutingCallbacks = false;

            CleanUp();
        }

        /// <summary>
        /// Report this promise's progress.
        /// </summary>
        /// <param name="progress">The progress amount (should be between 0 and 1)</param>
        public void ReportProgress(float progress)
        {
            if (State != PromiseState.Pending)
            {
                throw new PromiseStateException($"Cannot report progress on a promise in state ({State}), must be ({PromiseState.Pending})");
            }

            InvokeProgressCallbacks(progress);
        }

        internal void Set(PromiseState state, TValue? resolvedValue = default, Exception? rejectionReason = null)
        {
            State = state;
            ResolvedValue = resolvedValue;
            RejectionReason = rejectionReason;
        }

        internal void UseFulfiller(PromiseFulfiller resolver)
        {
            UseResolver((resolve, reject, progress) => resolver(
                () => resolve(default),
                reject
            ));
        }

        internal void UseFulfiller(PromiseFulfillerAndProgressor resolver)
        {
            UseResolver((resolve, reject, progress) => resolver(
                () => resolve(default),
                reject,
                progress
            ));
        }

        internal void UseResolver(PromiseResolver resolver)
        {
            UseResolver((resolve, reject, progress) => resolver(resolve, reject));
        }

        internal void UseResolver(PromiseResolverAndProgressor resolver)
        {
            if (State == PromiseState.Pending)
            {
                try
                {
                    resolver(ReportResolvedUnknown, ReportRejected, ReportProgress);
                }
                catch (Exception reason)
                {
                    ReportRejected(reason);
                }
            }
        }

        private IPromise<TConvertedValue> Invoke<TConvertedValue>(
            PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?>? onFulfilled,
            PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected,
            PromiseOnProgress? onProgress
        )
            => (IPromise<TConvertedValue>)Invoke<TConvertedValue>(
                (PromiseOnFulfillFunc<TValue, IPromise?>)(onFulfilled == null ? null : ((value) => onFulfilled(value))),
                (PromiseOnReject<IPromise?>)(onRejected == null ? null : ((reason) => onRejected(reason))),
                onProgress
            );

        private IPromise Invoke<TConvertedValue>(
            PromiseOnFulfillFunc<TValue, IPromise?>? onFulfilled,
            PromiseOnReject<IPromise?>? onRejected,
            PromiseOnProgress? onProgress
        )
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("Promise");
            }

            switch (State)
            {
                case PromiseState.Fulfilled:
                    try
                    {
                        var fulfilledResult = onFulfilled?.Invoke(ResolvedValue) ?? Promises<TConvertedValue>.Pool.Get();

                        if (fulfilledResult is IPromisePoolable poolableFulfilledResult)
                        {
                            poolableFulfilledResult.ParentInChain = this;
                        }

                        return fulfilledResult;
                    }
                    catch (Exception reason)
                    {
                        return Promise.Reject<TConvertedValue>(reason);
                    }

                case PromiseState.Rejected:
                    try
                    {
                        var rejectedResult = onRejected?.Invoke(RejectionReason) ?? Promises<TConvertedValue>.Pool.Get();

                        if (rejectedResult is IPromisePoolable poolableRejectedResult)
                        {
                            poolableRejectedResult.ParentInChain = this;
                        }

                        return rejectedResult;
                    }
                    catch (Exception reason)
                    {
                        return Promise.Reject<TConvertedValue>(reason);
                    }

                default:
                    var result = Promises<TConvertedValue>.Pool.Get();
                    result.ParentInChain = this;

                    if (onProgress != null)
                    {
                        AddOnProgressCallback(PromiseCallbackHandler.Create(result, onProgress));
                    }

                    AddOnFulfilledCallback(PromiseCallbackHandler.Create(result, (TValue? value) =>
                    {
                        if (onFulfilled == null)
                        {
                            if (result.State == PromiseState.Pending)
                            {
                                result.ReportResolved();
                            }
                        }
                        else
                        {
                            HandleSubresult(result, onFulfilled.Invoke(value));
                        }
                    }));

                    AddOnRejectedCallback(PromiseCallbackHandler.Create(result, (PromiseOnReject)((Exception reason) =>
                    {
                        if (onRejected == null)
                        {
                            if (result.State == PromiseState.Pending)
                            {
                                result.ReportRejected(reason);
                            }
                        }
                        else
                        {
                            HandleSubresult(result, onRejected.Invoke(reason));
                        }
                    })));

                    return result;
            }
        }

        private void InvokeLeaf(
            PromiseOnFulfillAction<TValue>? onFulfilled,
            PromiseOnReject? onRejected,
            PromiseOnProgress? onProgress,
            bool disposeChain
        )
        {
            if (Disposed)
            {
                throw new ObjectDisposedException("Promise");
            }

            switch (State)
            {
                case PromiseState.Fulfilled:
                    try
                    {
                        onFulfilled?.Invoke(ResolvedValue);
                        if (disposeChain)
                        {
                            FinishedWith = true;
                            Dispose();
                        }
                    }
                    catch (Exception unhandled)
                    {
                        throw new PromiseUnhandledException(unhandled);
                    }
                    return;

                case PromiseState.Rejected:
                    if (onRejected == null)
                    {
                        if (disposeChain)
                        {
                            throw new PromiseUnhandledException(RejectionReason);
                        }
                    }
                    else
                    {
                        try
                        {
                            onRejected.Invoke(RejectionReason);
                            if (disposeChain)
                            {
                                FinishedWith = true;
                                Dispose();
                            }
                        }
                        catch (Exception unhandled)
                        {
                            throw new PromiseUnhandledException(unhandled);
                        }
                    }
                    return;

                default:
                    if (onProgress != null)
                    {
                        AddOnProgressCallback(PromiseCallbackHandler.Create(onProgress));
                    }

                    if (onFulfilled == null)
                    {
                        if (disposeChain)
                        {
                            AddOnFulfilledCallback(PromiseCallbackHandler.Create((TValue? value) =>
                            {
                                FinishedWith = true;
                            }));
                        }
                    }
                    else
                    {
                        AddOnFulfilledCallback(PromiseCallbackHandler.Create((TValue? value) =>
                        {
                            onFulfilled(value);
                            if (disposeChain)
                            {
                                FinishedWith = true;
                            }
                        }));
                    }

                    if (onRejected == null)
                    {
                        if (disposeChain)
                        {
                            AddOnRejectedCallback(PromiseCallbackHandler.Create((unhandled) =>
                            {
                                throw new PromiseUnhandledException(unhandled);
                            }));
                        }
                    }
                    else
                    {
                        AddOnRejectedCallback(PromiseCallbackHandler.Create((reason) =>
                        {
                            onRejected(reason);
                            if (disposeChain)
                            {
                                FinishedWith = true;
                            }
                        }));
                    }
                    return;
            }
        }

        private void InvokeFulfilledCallbacks(TValue value)
        {
            if (FulfillCallbacks != null)
            {
                for (int i = 0; i < FulfillCallbacks.Count; i++)
                {
                    FulfillCallbacks[i].Invoke(value);
                }
            }
        }

        private void InvokeRejectCallbacks(Exception reason)
        {
            if (RejectionCallbacks != null)
            {
                for (int i = 0; i < RejectionCallbacks.Count; i++)
                {
                    RejectionCallbacks[i].Invoke(reason);
                }
            }
        }

        private void InvokeProgressCallbacks(float progress)
        {
            if (ProgressCallbacks != null)
            {
                for (int i = 0; i < ProgressCallbacks.Count; i++)
                {
                    ProgressCallbacks[i].Invoke(progress);
                }
            }
        }

        private void HandleSubresult<TConvertedValue>(Promise<TConvertedValue> result, IPromise? subresult)
        {
            if (subresult == null)
            {
                subresult = Promise.Resolve<TConvertedValue>();
            }

            if (subresult is IPromiseInternals subresultInternals)
            {
                result.InternalNode = subresultInternals;
            }

            result.ReportResolved(subresult);
        }

        void IPromisePoolable.AddChild(IPromisePoolable child)
        {
            if (ChildrenInChainFinished == null)
            {
                ChildrenInChainFinished = new Dictionary<IPromisePoolable, bool>();
            }

            ChildrenInChainFinished.Add(child, false);
        }

        void IPromisePoolable.ReportChildFinished(IPromisePoolable child)
        {
            Debug.Assert(ChildrenInChainFinished != null);
            ChildrenInChainFinished![child] = true;
            FinishedWith = ChildrenInChainFinished.Values.All((finishedWith) => finishedWith);

            if (FinishedWith && FulfillCallbacks == null && RejectionCallbacks == null)
            {
                Dispose();
            }
        }

        void IPromiseInternals.AddOnFulfilledCallback<TValueIn>(IPromiseCallbackHandler<TValueIn> fulfilledCallback)
        {
            if (!typeof(TValueIn).IsAssignableFrom(typeof(TValue)))
            {
                throw new PromiseTypeException();
            }
            AddOnFulfilledCallback((IPromiseCallbackHandler<TValue>)fulfilledCallback);
        }

        private void AddOnFulfilledCallback(IPromiseCallbackHandler<TValue> fulfilledCallback)
        {
            if (FulfillCallbacks == null)
            {
                FulfillCallbacks = new List<IPromiseCallbackHandler<TValue>>();
            }

            FulfillCallbacks.Add(fulfilledCallback);
        }

        void IPromiseInternals.AddOnProgressCallback(IPromiseCallbackHandler<float> progressCallback)
            => AddOnProgressCallback(progressCallback);

        private void AddOnProgressCallback(IPromiseCallbackHandler<float> progressCallback)
        {
            if (ProgressCallbacks == null)
            {
                ProgressCallbacks = new List<IPromiseCallbackHandler<float>>();
            }

            ProgressCallbacks.Add(progressCallback);
        }

        void IPromiseInternals.AddOnRejectedCallback(IPromiseCallbackHandler<Exception> rejectionCallback)
            => AddOnRejectedCallback(rejectionCallback);

        private void AddOnRejectedCallback(IPromiseCallbackHandler<Exception> rejectionCallback)
        {
            if (RejectionCallbacks == null)
            {
                RejectionCallbacks = new List<IPromiseCallbackHandler<Exception>>();
            }

            RejectionCallbacks.Add(rejectionCallback);
        }

        void IPromiseInternals.SyncWith(IPromiseInternals other)
        {
            switch (other.State)
            {
                case PromiseState.Fulfilled:
                    if (other.ResolvedValue is TValue resolvedTValue)
                    {
                        ReportResolved(resolvedTValue);
                        return;
                    }
                    if (other.ResolvedValue == null)
                    {
                        ReportResolved();
                        return;
                    }

                    throw new PromiseTypeException();

                case PromiseState.Rejected:
                    ReportRejected(other.RejectionReason);
                    return;

                default:
                    Synced = true;
                    other.AddOnProgressCallback(PromiseCallbackHandler.Create(this, (progress) => ReportProgress(progress)));
                    other.AddOnFulfilledCallback(PromiseCallbackHandler.Create(this, (TValue? value) => ReportFulfilled(value, false)));
                    other.AddOnRejectedCallback(PromiseCallbackHandler.Create(this, (reason) => ReportRejected(reason, false)));
                    return;
            }
        }

        public new string ToString()
        {
            var baseString = base.ToString();

            if (Disposed)
            {
                return $"{baseString}: Disposed";
            }

            return $"{baseString}: {State}";
        }
    }
}
