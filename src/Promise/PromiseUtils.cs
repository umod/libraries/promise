﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace uMod
{
    public static class Promise
    {
        /// <summary>
        /// Create a promise.
        /// </summary>
        public static ISettleablePromise Create()
            => Create<object?>();

        /// <summary>
        /// Create a promise.
        /// </summary>
        public static ISettleablePromise<TValue> Create<TValue>()
        {
            return Promises<TValue>.Pool.Get();
        }

        /// <summary>
        /// Create a promise with the given resolver.
        /// </summary>
        public static IPromise Create(PromiseFulfiller fulfiller)
        {
            var promise = Promises<object?>.Pool.Get();
            promise.UseFulfiller(fulfiller);
            return promise;
        }

        /// <summary>
        /// Create a promise with the given resolver.
        /// </summary>
        public static IPromise Create(PromiseFulfillerAndProgressor fulfiller)
        {
            var promise = Promises<object?>.Pool.Get();
            promise.UseFulfiller(fulfiller);
            return promise;
        }

        /// <summary>
        /// Create a promise with the given resolver.
        /// </summary>
        public static IPromise<TValue> Create<TValue>(PromiseResolver resolver)
        {
            var promise = Promises<TValue>.Pool.Get();
            promise.UseResolver(resolver);
            return promise;
        }

        /// <summary>
        /// Create a promise with the given resolver.
        /// </summary>
        public static IPromise<TValue> Create<TValue>(PromiseResolverAndProgressor resolver)
        {
            if (typeof(IPromise).IsAssignableFrom(typeof(TValue)))
            {
                throw new PromiseException("Trying to create a promise with a IPromise return type. Use Promise.Resolve() instead.");
            }
            var promise = Promises<TValue>.Pool.Get();
            promise.UseResolver(resolver);
            return promise;
        }

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise Resolved()
            => Resolve();

        /// <summary>
        /// Returns a new promise object that is resolved.
        /// </summary>
        public static IPromise Resolve()
        {
            return Resolve<object?>();
        }

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise<TValue> Resolved<TValue>(TValue? resolvedValue = default)
            => Resolve(resolvedValue);

        /// <summary>
        /// Returns a new promise object that is resolved with the given value.
        /// </summary>
        public static IPromise<TValue> Resolve<TValue>(TValue? resolvedValue = default)
        {
            if (resolvedValue is IPromise thenable)
            {
                return ResolveWithPromise<TValue>(thenable);
            }

            var promise = Promises<TValue>.Pool.Get();
            promise.Set(PromiseState.Fulfilled, resolvedValue);
            return promise;
        }

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise Resolved(IPromise syncWith)
            => Resolve(syncWith);

        /// <summary>
        /// Returns a new promise object that is resolved with the given thenable.
        /// When the given thenable settles, the returned promise settles the same way.
        /// </summary>
        public static IPromise Resolve(IPromise syncWith)
            => ResolveWithPromise<object?>(syncWith);

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise Resolved(ISettleablePromise syncWith)
            => Resolve(syncWith);

        /// <summary>
        /// Returns a new promise object that is resolved with the given promise.
        /// When the given promise settles, the returned promise settles the same way.
        /// </summary>
        public static IPromise Resolve(ISettleablePromise syncWith)
            => ResolveWithPromise<object?>(syncWith);

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise<TValue> Resolved<TValue>(IPromise<TValue> syncWith)
            => Resolve(syncWith);

        /// <summary>
        /// Returns a new promise object that is resolved with the given thenable.
        /// When the given thenable settles, the returned promise settles the same way.
        /// </summary>
        public static IPromise<TValue> Resolve<TValue>(IPromise<TValue> syncWith)
            => ResolveWithPromise<TValue>(syncWith);

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise<TValue> Resolved<TValue>(ISettleablePromise<TValue> syncWith)
            => Resolve(syncWith);

        /// <summary>
        /// Returns a new promise object that is resolved with the given promise.
        /// When the given promise settles, the returned promise settles the same way.
        /// </summary>
        public static IPromise<TValue> Resolve<TValue>(ISettleablePromise<TValue> syncWith)
            => ResolveWithPromise<TValue>(syncWith);

        [Obsolete("Use Promise.Resolve() instead.")]
        public static IPromise<TValue> Resolved<TValue>(Promise<TValue> syncWith)
            => Resolve(syncWith);

        /// <summary>
        /// Returns a new promise object that is resolved with the given promise.
        /// When the given promise settles, the returned promise settles the same way.
        /// </summary>
        public static IPromise<TValue> Resolve<TValue>(Promise<TValue> syncWith)
            => ResolveWithPromise<TValue>(syncWith);

        /// <summary>
        /// Returns a new promise object that is resolved with the given thenable.
        /// When the given thenable settles, the returned promise settles the same way.
        /// </summary>
        private static IPromise<TValue> ResolveWithPromise<TValue>(IPromise syncWith)
        {
            var promise = Promises<TValue>.Pool.Get();
            Sync(syncWith, promise);
            return promise;
        }

        [Obsolete("Use Promise.Reject() instead.")]
        public static IPromise Rejected(Exception rejectionException)
            => Reject(rejectionException);

        /// <summary>
        /// Returns a new promise object that is rejected with the given exception.
        /// </summary>
        public static IPromise Reject(Exception rejectionException)
        {
            return Reject<object?>(rejectionException);
        }

        [Obsolete("Use Promise.Reject() instead.")]
        public static IPromise<TValue> Rejected<TValue>(Exception rejectionException)
            => Reject<TValue>(rejectionException);

        /// <summary>
        /// Returns a new promise object that is rejected with the given exception.
        /// </summary>
        public static IPromise<TValue> Reject<TValue>(Exception rejectionException)
        {
            var promise = Promises<TValue>.Pool.Get();
            promise.Set(PromiseState.Rejected, default, rejectionException);
            return promise;
        }

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects. It rejects with the same reason as the rejected promise.
        /// </returns>
        public static IPromise All(params IPromise[] promises)
            => AllImpl<object?>(promises);

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects. It rejects with the same reason as the rejected promise.
        /// </returns>
        public static IPromise All(IEnumerable<IPromise> promises)
            => AllImpl<object?>(promises);

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating enumerable of the
        /// values from the resolved promises, in the same order as passed as parameters. If it
        /// rejects, it is rejected with the reason from the promise in the enumerable that was
        /// rejected first.
        /// </returns>
        public static IPromise<TValue[]> All<TValue>(params IPromise[] promises)
            => AllImpl<TValue>(promises);

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating enumerable of the
        /// values from the resolved promises, in the same order as defined in the given enumerable.
        /// If it rejects, it is rejected with the reason from the promise in the enumerable that
        /// was rejected first.
        /// </returns>
        public static IPromise<TValue[]> All<TValue>(IEnumerable<IPromise> promises)
            => AllImpl<TValue>(promises);

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating enumerable of the
        /// values from the resolved promises, in the same order as passed as parameters. If it
        /// rejects, it is rejected with the reason from the promise in the enumerable that was
        /// rejected first.
        /// </returns>
        public static IPromise<TValue[]> All<TValue>(params IPromise<TValue>[] promises)
            => AllImpl<TValue>(promises);

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating enumerable of the
        /// values from the resolved promises, in the same order as defined in the given enumerable.
        /// If it rejects, it is rejected with the reason from the promise in the enumerable that
        /// was rejected first.
        /// </returns>
        public static IPromise<TValue[]> All<TValue>(IEnumerable<IPromise<TValue>> promises)
            => AllImpl<TValue>((IEnumerable<IPromise>)promises);

        /// <summary>
        /// Wait for all promises to be resolved, or for any to be rejected.
        /// </summary>
        /// <returns>
        /// A new promise that fulfills when all given promises fulfill but rejects as soon as any
        /// of those promises rejects.
        ///
        /// If the returned promise resolves, it is resolved with an aggregating enumerable of the
        /// values from the resolved promises, in the same order as defined in the given enumerable.
        /// If it rejects, it is rejected with the reason from the promise in the enumerable that
        /// was rejected first.
        /// </returns>
        private static IPromise<TValue[]> AllImpl<TValue>(IEnumerable<IPromise> promises)
        {
            var promisesArray = promises.ToArray();
            if (promisesArray.Length == 0)
            {
                return Resolve(new TValue[0]);
            }

            var remainingCount = promisesArray.Length;
            var results = new TValue[promisesArray.Length];
            var progresses = new float[promisesArray.Length];
            var complete = false;

            return Create<TValue[]>((resolve, reject, progress) =>
            {
                for (var i = 0; i < promisesArray.Length; i++)
                {
                    // Save index to local var so it can be used in callbacks.
                    var index = i;

                    void onFullfull(object? result)
                    {
                        if (complete)
                        {
                            return;
                        }

                        --remainingCount;
                        progresses[index] = 1f;
                        if (result is TValue tResult)
                        {
                            results[index] = tResult;
                        }
                        if (remainingCount <= 0)
                        {
                            complete = true;
                            resolve(results);
                        }
                    }

                    void onReject(Exception reason)
                    {
                        if (complete)
                        {
                            return;
                        }
                        complete = true;
                        reject(reason);
                    }

                    void onProgress(float p)
                    {
                        if (!complete)
                        {
                            progresses[index] = p;
                            progress(progresses.Average());
                        }
                    }

                    if (promisesArray[index] is IPromiseInternals internals)
                    {
                        internals.DoneNonGeneric(onFullfull, onReject, onProgress);
                    }
                    else
                    {
                        promisesArray[index].Done(() => onFullfull(null), onReject, onProgress);
                    }
                }
            });
        }

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        public static IPromise<IPromiseAllSettledResult[]> AllSettled(params IPromise[] promises)
            => AllSettledImpl<object?>(promises);

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        public static IPromise<IPromiseAllSettledResult[]> AllSettled(IEnumerable<IPromise> promises)
            => AllSettledImpl<object?>(promises);

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        public static IPromise<IPromiseAllSettledResult[]> AllSettled<TValue>(params IPromise[] promises)
            => AllSettledImpl<TValue>(promises);

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        public static IPromise<IPromiseAllSettledResult[]> AllSettled<TValue>(IEnumerable<IPromise> promises)
            => AllSettledImpl<TValue>(promises);

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        public static IPromise<IPromiseAllSettledResult[]> AllSettled<TValue>(params IPromise<TValue>[] promises)
            => AllSettledImpl<TValue>(promises);

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        public static IPromise<IPromiseAllSettledResult[]> AllSettled<TValue>(IEnumerable<IPromise<TValue>> promises)
            => AllSettledImpl<TValue>((IEnumerable<IPromise>)promises);

        /// <summary>
        /// Wait until all promises have settled (each may resolve or reject).
        /// </summary>
        /// <returns>
        /// A Promise that resolves after all of the given promises have either resolved or
        /// rejected, with an enumerable of objects that each describe the outcome of each promise.
        /// </returns>
        private static IPromise<IPromiseAllSettledResult[]> AllSettledImpl<TValue>(IEnumerable<IPromise> promises)
        {
            var promisesArray = promises.ToArray();
            if (promisesArray.Length == 0)
            {
                return Resolve(new IPromiseAllSettledResult[0]);
            }

            var remainingCount = promisesArray.Length;
            var results = new IPromiseAllSettledResult[promisesArray.Length];
            var progresses = new float[promisesArray.Length];
            var complete = false;

            return Create<IPromiseAllSettledResult[]>((resolve, reject, progress) =>
            {
                for (var i = 0; i < promisesArray.Length; i++)
                {
                    // Save index to local var so it can be used in callbacks.
                    var index = i;

                    void onFullfull(object? result)
                    {
                        if (complete)
                        {
                            return;
                        }

                        --remainingCount;
                        progresses[index] = 1f;
                        results[index] = result is TValue tResult
                            ? new PromiseAllSettledResultFulfilled<TValue>(tResult)
                            : new PromiseAllSettledResultFulfilled();
                        if (remainingCount <= 0)
                        {
                            complete = true;
                            resolve(results);
                        }
                    }

                    void onReject(Exception reason)
                    {
                        if (complete)
                        {
                            return;
                        }

                        --remainingCount;
                        results[index] = new PromiseAllSettledResultRejected(reason);
                        progresses[index] = 1f;
                        progress(progresses.Average());
                        if (remainingCount <= 0)
                        {
                            complete = true;
                            resolve(results);
                        }
                    }

                    void onProgress(float p)
                    {
                        if (complete)
                        {
                            return;
                        }

                        progresses[index] = p;
                        progress(progresses.Average());
                    }

                    if (promisesArray[index] is IPromiseInternals internals)
                    {
                        internals.DoneNonGeneric(onFullfull, onReject, onProgress);
                    }
                    else
                    {
                        promisesArray[index].Done(() => onFullfull(null), onReject, onProgress);
                    }
                }
            });
        }

        /// <summary>
        /// Wait until one of the promises resolves.
        /// </summary>
        public static IPromise Any(params IPromise[] promises)
            => AnyImpl<object?>(promises);

        /// <summary>
        /// Wait until any one of the promises fulfills.
        /// </summary>
        public static IPromise Any(IEnumerable<IPromise> promises)
            => AnyImpl<object?>(promises);

        /// <summary>
        /// Wait until any one of the promises fulfills.
        /// </summary>
        /// <returns>
        /// A Promise that resolves to the same value of the promise that fulfilled first. It
        /// rejects if all given promises reject. And throws if no promises are passed.
        /// </returns>
        public static IPromise<TValue> Any<TValue>(params IPromise[] promises)
            => AnyImpl<TValue>(promises);

        /// <summary>
        /// Wait until any one of the promises fulfills.
        /// </summary>
        /// <returns>
        /// A Promise that resolves to the same value of the promise that fulfilled first. It
        /// rejects if all given promises reject. And throws if no promises are passed.
        /// </returns>
        public static IPromise<TValue> Any<TValue>(IEnumerable<IPromise> promises)
            => AnyImpl<TValue>(promises);

        /// <summary>
        /// Wait until any one of the promises fulfills.
        /// </summary>
        /// <returns>
        /// A Promise that resolves to the same value of the promise that fulfilled first. It
        /// rejects if all given promises reject. And throws if no promises are passed.
        /// </returns>
        public static IPromise<TValue> Any<TValue>(params IPromise<TValue>[] promises)
            => AnyImpl<TValue>(promises);

        /// <summary>
        /// Wait until any one of the promises fulfills.
        /// </summary>
        /// <returns>
        /// A Promise that resolves to the same value of the promise that fulfilled first. It
        /// rejects if all given promises reject. And throws if no promises are passed.
        /// </returns>
        public static IPromise<TValue> Any<TValue>(IEnumerable<IPromise<TValue>> promises)
            => AnyImpl<TValue>((IEnumerable<IPromise>)promises);

        /// <summary>
        /// Wait until any one of the promises fulfills.
        /// </summary>
        /// <returns>
        /// A Promise that resolves to the same value of the promise that fulfilled first. It
        /// rejects if all given promises reject. And throws if no promises are passed.
        /// </returns>
        private static IPromise<TValue> AnyImpl<TValue>(IEnumerable<IPromise> promises)
        {
            var promisesArray = promises.ToArray();
            if (promisesArray.Length == 0)
            {
                return Reject<TValue>(new PromiseException("No promises."));
            }

            var remainingCount = promisesArray.Length;
            var complete = false;

            return Create<TValue>((resolve, reject) =>
            {
                for (var i = 0; i < promisesArray.Length; i++)
                {
                    // Save index to local var so it can be used in callbacks.
                    var index = i;

                    void onFullfull(object? result)
                    {
                        if (complete)
                        {
                            return;
                        }
                        complete = true;
                        resolve(result is TValue tResult ? tResult : default);
                    }

                    void onReject(Exception reason)
                    {
                        if (complete)
                        {
                            return;
                        }
                        --remainingCount;
                        if (remainingCount <= 0)
                        {
                            complete = true;
                            reject(new PromiseException("All promises were rejected."));
                        }
                    }

                    if (promisesArray[index] is IPromiseInternals internals)
                    {
                        internals.DoneNonGeneric(onFullfull, onReject);
                    }
                    else
                    {
                        promisesArray[index].Done(
                            () => onFullfull(null),
                            onReject
                        );
                    }
                }
            });
        }

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        public static IPromise Race(params IPromise[] promises)
            => RaceImpl<object?>(promises);

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        public static IPromise Race(IEnumerable<IPromise> promises)
            => RaceImpl<object?>(promises);

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        /// <returns>
        /// A Promise that settles the same way as the promise in the enumerable that settled first.
        /// </returns>
        public static IPromise<TValue> Race<TValue>(params IPromise[] promises)
            => RaceImpl<TValue>(promises);

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        /// <returns>
        /// A Promise that settles the same way as the promise in the enumerable that settled first.
        /// </returns>
        public static IPromise<TValue> Race<TValue>(IEnumerable<IPromise> promises)
            => RaceImpl<TValue>(promises);

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        /// <returns>
        /// A Promise that settles the same way as the promise in the enumerable that settled first.
        /// </returns>
        public static IPromise<TValue> Race<TValue>(params IPromise<TValue>[] promises)
            => RaceImpl<TValue>(promises);

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        /// <returns>
        /// A Promise that settles the same way as the promise in the enumerable that settled first.
        /// </returns>
        public static IPromise<TValue> Race<TValue>(IEnumerable<IPromise<TValue>> promises)
            => RaceImpl<TValue>((IEnumerable<IPromise>)promises);

        /// <summary>
        /// Wait until one of the promises fulfills or rejects.
        /// </summary>
        /// <returns>
        /// A Promise that settles the same way as the promise in the enumerable that settled first.
        /// </returns>
        private static IPromise<TValue> RaceImpl<TValue>(IEnumerable<IPromise> promises)
        {
            var promisesArray = promises.ToArray();
            if (promisesArray.Length == 0)
            {
                throw new ArgumentException("Empty enumerable.");
            }

            var complete = false;

            return Create<TValue>((resolve, reject) =>
            {
                for (var i = 0; i < promisesArray.Length; i++)
                {
                    // Save index to local var so it can be used in callbacks.
                    var index = i;

                    void onFullfull(object? result)
                    {
                        if (complete)
                        {
                            return;
                        }
                        complete = true;
                        resolve(result is TValue tResult ? tResult : default);
                    }

                    void onReject(Exception reason)
                    {
                        if (complete)
                        {
                            return;
                        }
                        complete = true;
                        reject(reason);
                    }

                    if (promisesArray[index] is IPromiseInternals internals)
                    {
                        internals.DoneNonGeneric(onFullfull, onReject);
                    }
                    else
                    {
                        promisesArray[index].Done(
                            () => onFullfull(null),
                            onReject
                        );
                    }
                }
            });
        }

        /// <summary>
        /// Create a one-way sync between the two given promises.
        ///
        /// When the independent promise settles, the dependent promise will be settled the same way.
        /// </summary>
        /// <param name="independent">The promise that will settle independently.</param>
        /// <param name="dependent">The promise that will settle when the independent promise resolves.</param>

        internal static void Sync<TValue>(IPromise independent, ISettleablePromise dependent)
        {
            if (independent is IPromiseInternals independentWrapper && dependent is IPromiseInternals dependentWrapper)
            {
                dependentWrapper.SyncWith(independentWrapper);
                return;
            }

            independent.ThenPeek(
                () => dependent.ReportResolved(),
                (reason) => dependent.ReportRejected(reason),
                (progress) => dependent.ReportProgress(progress)
            );
        }

        /// <summary>
        /// Create a one-way sync between the two given promises.
        ///
        /// When the independent promise settles, the dependent promise will be settled the same way.
        /// </summary>
        /// <param name="independent">The promise that will settle independently.</param>
        /// <param name="dependent">The promise that will settle when the independent promise resolves.</param>
        internal static void Sync<TValue>(IPromise<TValue> independent, ISettleablePromise<TValue> dependent)
        {
            if (independent is IPromiseInternals independentWrapper && dependent is IPromiseInternals dependentWrapper)
            {
                dependentWrapper.SyncWith(independentWrapper);
                return;
            }

            independent.ThenPeek(
                (value) => dependent.ReportResolved(value),
                (reason) => dependent.ReportRejected(reason),
                (progress) => (dependent).ReportProgress(progress)
            );
        }

        /// <summary>
        /// Create a one-way sync between the two given promises.
        ///
        /// When the independent promise settles, the dependent promise will be settled the same way.
        /// </summary>
        /// <param name="independent">The promise that will settle independently.</param>
        /// <param name="dependent">The promise that will settle when the independent promise resolves.</param>
        internal static void Sync<TValue>(IPromise independent, ISettleablePromise<TValue> dependent)
        {
            if (independent is IPromiseInternals independentWrapper && dependent is IPromiseInternals dependentWrapper)
            {
                dependentWrapper.SyncWith(independentWrapper);
                return;
            }
            if (independent is IPromise<TValue> independentGeneric)
            {
                Sync(independentGeneric, dependent);
                return;
            }

            throw new PromiseTypeException();
        }
    }
}
