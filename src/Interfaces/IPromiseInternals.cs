﻿using System;

namespace uMod
{
    internal interface IPromiseInternals : IRejectable, IPromisePoolable
    {
        PromiseState State { get; }
        object? ResolvedValue { get; }
        Exception? RejectionReason { get; }

        bool FinishedWith { get; set; }
        bool ExecutingCallbacks { get; }

        void AddOnFulfilledCallback<TValue>(IPromiseCallbackHandler<TValue?> fulfilledCallback);

        void AddOnProgressCallback(IPromiseCallbackHandler<float> progressCallback);

        void AddOnRejectedCallback(IPromiseCallbackHandler<Exception> rejectionCallback);

        void SyncWith(IPromiseInternals independent);

        void DoneNonGeneric(PromiseOnFulfillAction<object?>? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null);
    }
}
