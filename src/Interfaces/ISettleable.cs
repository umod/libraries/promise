﻿namespace uMod
{
    public interface ISettleable : IResolveable, IRejectable
    {
    }

    public interface ISettleable<in TValue> : ISettleable, IResolveable<TValue>
    {
    }
}
