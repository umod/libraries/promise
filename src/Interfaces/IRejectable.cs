﻿using System;

namespace uMod
{
    public interface IRejectable
    {
        /// <summary>
        /// Reject this.
        /// </summary>
        /// <param name="reason">The reason for the rejection</param>
        void ReportRejected(Exception reason);
    }
}
