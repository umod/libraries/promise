﻿using System;

namespace uMod
{
    public interface IPromise<out TValue> : IPromise
    {
        /// <summary>
        /// Appends a progress handler callback to the promise.
        /// </summary>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>The promise (this).</returns>
        new IPromise<TValue> Progress(PromiseOnProgress onProgress);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillAction<TValue> onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillFunc<TValue, IPromise?> onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillAction<TValue> onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillFunc<TValue, IPromise?> onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, TConvertedValue> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, TConvertedValue> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TValue, IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        new IPromise<TValue> ThenPeek(PromiseOnFulfillAction? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise<TValue> ThenPeek(PromiseOnFulfillAction<TValue>? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.<br/>
        /// But unlike the Then method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        void Done(PromiseOnFulfillAction<TValue>? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a rejection handlers to the promise but returns the same promise instead of creating
        /// a new one.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        new IPromise<TValue> CatchPeek(PromiseOnReject onRejected);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        new IPromise<TValue> FinallyPeek(PromiseOnSettled onSettled);
    }
}
