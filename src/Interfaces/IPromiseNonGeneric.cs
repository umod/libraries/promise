﻿using System;

namespace uMod
{
    public interface IPromise
    {
        /// <summary>
        /// Appends a progress handler callback to the promise.
        /// </summary>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>The promise (this).</returns>
        IPromise Progress(PromiseOnProgress onProgress);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillAction onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillFunc<IPromise?> onFulfilled, PromiseOnReject? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillAction onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise Then(PromiseOnFulfillFunc<IPromise?> onFulfilled, PromiseOnReject<IPromise?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TConvertedValue> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<TConvertedValue> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<TConvertedValue>? onRejected, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// A new promise resolving to the return value of the fulfillment or rejection handler.
        /// </returns>
        IPromise<TConvertedValue> Then<TConvertedValue>(PromiseOnFulfillFunc<IPromise<TConvertedValue?>?> onFulfilled, PromiseOnReject<IPromise<TConvertedValue?>?>? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise ThenPeek(PromiseOnFulfillAction? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a fulfillment, rejection and/or progress handlers to the promise.<br/>
        /// But unlike the Then method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// </summary>
        /// <param name="onFulfilled">The fulfillment handler.</param>
        /// <param name="onRejected">The rejection handler.</param>
        /// <param name="onProgress">The progress handler.</param>
        void Done(PromiseOnFulfillAction? onFulfilled = null, PromiseOnReject? onRejected = null, PromiseOnProgress? onProgress = null);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        IPromise Catch(PromiseOnReject onRejected);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        IPromise Catch(PromiseOnReject<IPromise?> onRejected);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        IPromise<TConvertedValue> Catch<TConvertedValue>(PromiseOnReject<TConvertedValue> onRejected);

        /// <summary>
        /// Appends a rejection handler callback to the promise.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>A new promise resolving to the return value of the callback if it is called.</returns>
        IPromise<TConvertedValue> Catch<TConvertedValue>(PromiseOnReject<IPromise<TConvertedValue?>?> onRejected);

        /// <summary>
        /// Appends a rejection handlers to the promise but returns the same promise instead of creating
        /// a new one.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise CatchPeek(PromiseOnReject onRejected);

        /// <summary>
        /// Appends a rejection handler to the promise.<br/>
        /// But unlike the Catch method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// </summary>
        /// <param name="onRejected">The rejection handler.</param>
        void Fail(PromiseOnReject onRejected);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>A new promise that is resolved when the original promise is resolved.</returns>
        IPromise Finally(PromiseOnSettled onSettled);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>A new promise that is resolved when the original promise is resolved.</returns>
        IPromise Finally(PromiseOnSettled<IPromise?> onSettled);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>
        /// A new promise that is resolved when the original promise is resolved; resolving to the
        /// return value of the callback.
        /// </returns>
        IPromise<TConvertedValue> Finally<TConvertedValue>(PromiseOnSettled<TConvertedValue> onSettled);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled, whether
        /// fulfilled or rejected.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>
        /// A new promise that is resolved when the original promise is resolved; resolving to the
        /// return value of the callback.
        /// </returns>
        IPromise<TConvertedValue> Finally<TConvertedValue>(PromiseOnSettled<IPromise<TConvertedValue?>?> onSettled);

        /// <summary>
        /// Appends a handler to the promise that is called when the promise is settled but returns
        /// the same promise instead of creating a new one.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        /// <returns>
        /// This.
        /// </returns>
        IPromise FinallyPeek(PromiseOnSettled onSettled);

        /// <summary>
        /// Appends a settled handler to the promise.<br/>
        /// But unlike the Finally method, this method is a promise chain terminator.
        /// <para/>
        /// If there are any unhandled exceptions, a PromiseUnhandledException will be thrown.<br/>
        /// Because of this, it is recommended to always end a promise chain with all call to a
        /// promise chain terminator method to ensure that no unexpected exceptions are quietly
        /// being ignored.
        /// </summary>
        /// <param name="onSettled">The handler.</param>
        void Complete(PromiseOnSettled onSettled);

        /// <summary>
        /// Allows for the Promise to be joined, the signal of the event is only set when the
        /// Promise reaches a conclusion of Rejected or Resolved
        /// <para/>
        /// Returns if the Promise resolved before the timeout
        /// <para/>
        /// Returns true when a negative or no timeout is given
        /// </summary>
        bool Join(int timeout = -1);
    }
}
