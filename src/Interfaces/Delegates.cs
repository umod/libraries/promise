using System;

namespace uMod
{
    public delegate void PromiseFulfill();

    public delegate void PromiseFulfill<in TValue>(TValue value);

    public delegate void PromiseResolve(IPromise value);

    public delegate void PromiseResolve<in TValue>(IPromise<TValue> value);

    public delegate void PromiseResolveUnknown(object? value = default);

    public delegate void PromiseReject(Exception reason);

    public delegate void PromiseProgress(float progression);

    public delegate void PromiseFulfiller(PromiseFulfill resolve, PromiseReject reject);

    public delegate void PromiseFulfillerAndProgressor(PromiseFulfill resolve, PromiseReject reject, PromiseProgress progress);

    // not used
    delegate void PromiseFulfiller<out TValue>(PromiseFulfill<TValue> resolve, PromiseReject reject);

    // not used
    delegate void PromiseFulfillerAndProgressor<out TValue>(PromiseFulfill<TValue> resolve, PromiseReject reject, PromiseProgress progress);

    public delegate void PromiseResolver(PromiseResolveUnknown resolve, PromiseReject reject);

    public delegate void PromiseResolverAndProgressor(PromiseResolveUnknown resolve, PromiseReject reject, PromiseProgress progress);

    public delegate void PromiseOnFulfillAction();

    public delegate void PromiseOnFulfillAction<in TValue>(TValue value);

    public delegate TResult PromiseOnFulfillFunc<out TResult>();

    public delegate TResult PromiseOnFulfillFunc<in TValue, out TResult>(TValue value);

    public delegate void PromiseOnReject(Exception reason);

    public delegate TResult PromiseOnReject<out TResult>(Exception reason);

    public delegate void PromiseOnSettled();

    public delegate TResult PromiseOnSettled<out TResult>();

    public delegate void PromiseOnProgress(float progress);
}
