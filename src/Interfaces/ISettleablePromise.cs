﻿namespace uMod
{
    public interface ISettleablePromise : ISettleable, IPromise
    {
    }

    public interface ISettleablePromise<TValue> : ISettleablePromise, ISettleable<TValue>, IPromise<TValue>
    {
    }
}
