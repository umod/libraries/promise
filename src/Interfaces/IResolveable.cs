﻿using System;

namespace uMod
{
    public interface IResolveable
    {
        /// <summary>
        /// Report this' progress.
        /// </summary>
        /// <param name="progress">The progress amount (should be between 0 and 1)</param>
        void ReportProgress(float progress);

        /// <summary>
        /// Resolve this.
        /// </summary>
        void ReportResolved();

        /// <summary>
        /// Resolve this.
        /// </summary>
        /// <param name="value">A promise to match the behavior of.</param>
        void ReportResolved(IPromise resolveWith);
    }

    public interface IResolveable<in TValue> : IResolveable
    {
        /// <summary>
        /// Resolve this.
        /// </summary>
        /// <param name="value">The value to resolve with.</param>
        void ReportResolved(TValue? value);

        /// <summary>
        /// Resolve this.
        /// </summary>
        /// <param name="value">A promise to match the behavior of.</param>
        void ReportResolved(IPromise<TValue?> resolveWith);
    }
}
