﻿using System;

namespace uMod
{
    internal interface IPromisePoolable
    {
        void Init();

        void Dispose();

        IPromisePoolable ParentInChain { set; }

        void AddChild(IPromisePoolable child);

        void ReportChildFinished(IPromisePoolable child);
    }
}
