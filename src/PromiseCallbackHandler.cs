﻿using System;
using System.Diagnostics;

namespace uMod
{
    internal interface IPromiseCallbackHandler<in TValue>
    {
        void Invoke(TValue value);
    }

    internal static class PromiseCallbackHandler
    {
        public static PromiseCallbackHandler<TValue?> Create<TValue>(PromiseOnFulfillAction<TValue?> callback)
            => Create(null, callback);

        public static PromiseCallbackHandler<TValue?> Create<TValue>(IRejectable? rejectable, PromiseOnFulfillAction<TValue?> callback)
        {
            return new PromiseCallbackHandler<TValue?>(rejectable, (TValue? value) => callback(value));
        }

        public static PromiseCallbackHandler<Exception> Create(PromiseOnReject callback)
            => Create(null, callback);

        public static PromiseCallbackHandler<Exception> Create(IRejectable? rejectable, PromiseOnReject callback)
        {
            return new PromiseCallbackHandler<Exception>(rejectable, (Exception reason) => callback(reason));
        }

        public static PromiseCallbackHandler<float> Create(PromiseOnProgress callback)
            => Create(null, callback);

        public static PromiseCallbackHandler<float> Create(IRejectable? rejectable, PromiseOnProgress callback)
        {
            return new PromiseCallbackHandler<float>(rejectable, (float progression) => callback(progression));
        }
    }

    internal class PromiseCallbackHandler<TValue> : IPromiseCallbackHandler<TValue>
    {
        private Action<TValue> Callback;
        private IRejectable? Rejectable;

        internal PromiseCallbackHandler(IRejectable? rejectable, Action<TValue> callback)
        {
            Rejectable = rejectable;
            Callback = callback;
        }

        public void Invoke(TValue value)
        {
            try
            {
                Callback(value);
            }
            catch (PromiseException)
            {
                throw;
            }
            catch (Exception exception)
            {
                if (Rejectable != null)
                {
                    Rejectable.ReportRejected(exception);
                }
                else
                {
                    throw new PromiseUnhandledException(exception);
                }
            }
        }
    }
}
