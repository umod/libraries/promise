﻿using System;

namespace uMod
{
    public enum PromiseAllSettledResultStatus
    {
        Fulfilled,
        Rejected
    }

    public interface IPromiseAllSettledResult
    {
        PromiseAllSettledResultStatus Status { get; }
    }

    public class PromiseAllSettledResultFulfilled : IPromiseAllSettledResult
    {
        public PromiseAllSettledResultStatus Status => PromiseAllSettledResultStatus.Fulfilled;
    }

    public class PromiseAllSettledResultFulfilled<TValue> : IPromiseAllSettledResult
    {
        public readonly TValue Value;

        internal PromiseAllSettledResultFulfilled(TValue value)
        {
            Value = value;
        }

        public PromiseAllSettledResultStatus Status => PromiseAllSettledResultStatus.Fulfilled;
    }

    public class PromiseAllSettledResultRejected : IPromiseAllSettledResult
    {
        public readonly Exception Reason;

        internal PromiseAllSettledResultRejected(Exception reason)
        {
            Reason = reason;
        }

        public PromiseAllSettledResultStatus Status => PromiseAllSettledResultStatus.Rejected;
    }
}
