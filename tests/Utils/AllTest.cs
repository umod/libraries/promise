﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace uMod.Tests.Utils
{
    [TestClass]
    public class AllTest
    {
        [TestMethod]
        public void PromiseSameType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var all = Promise.All(promise1, promise2);

            var completed = 0;

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });

            promise1.ReportResolved(1);
            promise2.ReportResolved(2);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAll2MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();

            var all = Promise.All<object>(promise1, promise2);

            var completed = 0;

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, (int)values[0]);
                Assert.AreEqual("foo", (string)values[1]);
            });

            promise1.ReportResolved(1);
            promise2.ReportResolved("foo");

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAll3MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();
            var promise3 = Promise.Create<bool>();

            var all = Promise.All<object>(promise1, promise2, promise3);

            var completed = 0;

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(3, values.Length);
                Assert.AreEqual(1, (int)values[0]);
                Assert.AreEqual("foo", (string)values[1]);
                Assert.AreEqual(true, (bool)values[2]);
            });

            promise1.ReportResolved(1);
            promise2.ReportResolved("foo");
            promise3.ReportResolved(true);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAll8MixedType()
        {
            var promise0 = Promise.Create<int>();
            var promise1 = Promise.Create<string>();
            var promise2 = Promise.Create<bool>();
            var promise3 = Promise.Create<float>();
            var promise4 = Promise.Create<double>();
            var promise5 = Promise.Create<long>();
            var promise6 = Promise.Create<char>();
            var promise7 = Promise.Create();

            var progress = 0f;

            var all = Promise.All<object>(promise0, promise1, promise2, promise3, promise4, promise5, promise6, promise7)
                .Progress(p => { progress = p; });

            var completed = 0;

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(8, values.Length);

                Assert.AreEqual(1, values[0]);
                Assert.AreEqual("foo", values[1]);
                Assert.AreEqual(true, values[2]);
                Assert.AreEqual(2f, values[3]);
                Assert.AreEqual(3d, values[4]);
                Assert.AreEqual(4L, values[5]);
                Assert.AreEqual('A', values[6]);
                Assert.AreEqual(default, values[7]);
            });

            Assert.AreEqual(0f, progress);
            promise0.ReportResolved(1);
            Assert.AreEqual(1f / 8f, progress);
            promise1.ReportResolved("foo");
            Assert.AreEqual(2f / 8f, progress);
            promise2.ReportResolved(true);
            Assert.AreEqual(3f / 8f, progress);
            promise3.ReportResolved(2f);
            Assert.AreEqual(4f / 8f, progress);
            promise4.ReportResolved(3d);
            Assert.AreEqual(5f / 8f, progress);
            promise5.ReportResolved(4L);
            Assert.AreEqual(6f / 8f, progress);
            promise6.ReportResolved('A');
            Assert.AreEqual(7f / 8f, progress);
            promise7.ReportResolved();
            Assert.AreEqual(1f, progress);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve(1);
            var promise2 = Promise.Resolve(2);

            var all = Promise.All<int>(promise1, promise2);

            var completed = 0;
            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllGenericEmptyCase()
        {
            var all = Promise.All<int>(Enumerable.Empty<IPromise>());

            var completed = 0;
            all.Done(values =>
            {
                ++completed;
                Assert.AreEqual(0, values.Count());
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllGenericRejects()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var progress = 0f;

            var all = Promise.All<int>(promise1, promise2).Progress(p => { progress = p; });

            var expectedException = new Exception();

            var errors = 0;
            all.Done(
                values => Assert.Fail(),
                e =>
                {
                    ++errors;
                    Assert.AreEqual(expectedException, e);
                }
            );

            Assert.AreEqual(0f, progress);

            promise1.ReportRejected(expectedException);

            Assert.AreEqual(0f, progress);

            promise2.ReportResolved(2);

            Assert.AreEqual(0f, progress);
            Assert.AreEqual(1, errors);
        }

        public void PromiseAllGenericSingleType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var progress1 = 0f;
            var progress2 = 0f;

            var all1 = Promise.All<int>(new ISettleablePromise<int>[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var all2 = Promise.All<int>(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            all1.Done(values =>
            {
                ++callback1;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });
            all2.Done(values =>
            {
                ++callback2;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved(1);

            Assert.AreEqual(0.5f, progress1);
            Assert.AreEqual(0.5f, progress2);

            promise2.ReportResolved(2);

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseAllNonGeneric()
        {
            var promise1 = Promise.Create();
            var promise2 = Promise.Create();

            var progress1 = 0f;
            var progress2 = 0f;

            var all1 = Promise.All(new ISettleablePromise[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var all2 = Promise.All(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            all1.Done(() => { ++callback1; });
            all2.Done(() => { ++callback2; });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved();

            Assert.AreEqual(0.5f, progress1);
            Assert.AreEqual(0.5f, progress2);

            promise2.ReportResolved();

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseAllNonGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve();
            var promise2 = Promise.Resolve();

            var all = Promise.All(promise1, promise2);

            var completed = 0;
            all.Done(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllNonGenericEmptyCase()
        {
            var all = Promise.All();

            var completed = 0;
            all.Done(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllNonGenericRejects()
        {
            var promise1 = Promise.Create();
            var promise2 = Promise.Create();

            var progress = 0f;

            var all = Promise.All(promise1, promise2).Progress(p => { progress = p; });

            var expectedException = new Exception();

            var errors = 0;
            all.Done(
                () => Assert.Fail(),
                e =>
                {
                    ++errors;
                    Assert.AreEqual(expectedException, e);
                }
            );

            Assert.AreEqual(0f, progress);

            promise1.ReportResolved();

            Assert.AreEqual(0.5f, progress);

            promise2.ReportRejected(expectedException);

            Assert.AreEqual(0.5f, progress);
            Assert.AreEqual(1, errors);
        }
    }
}
