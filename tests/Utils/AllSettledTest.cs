﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace uMod.Tests.Utils
{
    [TestClass]
    public class AllSettledTest
    {
        [TestMethod]
        public void PromiseSameType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var all = Promise.AllSettled(promise1, promise2);

            var completed = 0;
            var ex = new Exception();

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, ((PromiseAllSettledResultFulfilled<int>)values[0]).Value);
                Assert.AreEqual(ex, ((PromiseAllSettledResultRejected)values[1]).Reason);
            });

            promise1.ReportResolved(1);
            promise2.ReportRejected(ex);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllSettled2MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();

            var all = Promise.AllSettled<object>(promise1, promise2);

            var completed = 0;
            var ex = new Exception();

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, (int)((PromiseAllSettledResultFulfilled<object>)values[0]).Value);
                Assert.AreEqual(ex, ((PromiseAllSettledResultRejected)values[1]).Reason);
            });

            promise1.ReportResolved(1);
            promise2.ReportRejected(ex);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllSettled3MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();
            var promise3 = Promise.Create<bool>();

            var all = Promise.AllSettled<object>(promise1, promise2, promise3);

            var completed = 0;
            var ex = new Exception();

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(3, values.Length);
                Assert.AreEqual(1, (int)((PromiseAllSettledResultFulfilled<object>)values[0]).Value);
                Assert.AreEqual(ex, ((PromiseAllSettledResultRejected)values[1]).Reason);
                Assert.AreEqual(true, (bool)((PromiseAllSettledResultFulfilled<object>)values[2]).Value);
            });

            promise1.ReportResolved(1);
            promise2.ReportRejected(ex);
            promise3.ReportResolved(true);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllSettled8MixedType()
        {
            var promise0 = Promise.Create<int>();
            var promise1 = Promise.Create<string>();
            var promise2 = Promise.Create<bool>();
            var promise3 = Promise.Create<float>();
            var promise4 = Promise.Create<double>();
            var promise5 = Promise.Create();
            var promise6 = Promise.Create<char>();
            var promise7 = Promise.Create();

            var progress = 0f;

            var all = Promise.AllSettled<object>(promise0, promise1, promise2, promise3, promise4, promise5, promise6, promise7)
                .Progress(p => { progress = p; });

            var completed = 0;
            var ex1 = new Exception();
            var ex2 = new Exception();

            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(8, values.Length);

                Assert.AreEqual(1, (int)((PromiseAllSettledResultFulfilled<object>)values[0]).Value);
                Assert.AreEqual("foo", (string)((PromiseAllSettledResultFulfilled<object>)values[1]).Value);
                Assert.AreEqual(true, (bool)((PromiseAllSettledResultFulfilled<object>)values[2]).Value);
                Assert.AreEqual(2f, (float)((PromiseAllSettledResultFulfilled<object>)values[3]).Value);
                Assert.AreEqual(ex1, ((PromiseAllSettledResultRejected)values[4]).Reason);
                var canBeCastCheck = (PromiseAllSettledResultFulfilled)values[5];
                Assert.AreEqual('A', (char)((PromiseAllSettledResultFulfilled<object>)values[6]).Value);
                Assert.AreEqual(ex2, ((PromiseAllSettledResultRejected)values[7]).Reason);
            });

            Assert.AreEqual(0f, progress);
            promise0.ReportResolved(1);
            Assert.AreEqual(1f / 8, progress);
            promise1.ReportResolved("foo");
            Assert.AreEqual(2f / 8, progress);
            promise2.ReportResolved(true);
            Assert.AreEqual(3f / 8, progress);
            promise3.ReportResolved(2f);
            Assert.AreEqual(4f / 8, progress);
            promise4.ReportRejected(ex1);
            Assert.AreEqual(5f / 8, progress);
            promise5.ReportResolved();
            Assert.AreEqual(6f / 8, progress);
            promise6.ReportResolved('A');
            Assert.AreEqual(7f / 8, progress);
            promise7.ReportRejected(ex2);
            Assert.AreEqual(1f, progress);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllSettledGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve(1);
            var promise2 = Promise.Resolve(2);

            var all = Promise.AllSettled(promise1, promise2);

            var completed = 0;
            all.Done(values =>
            {
                ++completed;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(PromiseAllSettledResultStatus.Fulfilled, values[0].Status);
                Assert.AreEqual(1, ((PromiseAllSettledResultFulfilled<int>)values[0]).Value);
                Assert.AreEqual(PromiseAllSettledResultStatus.Fulfilled, values[1].Status);
                Assert.AreEqual(2, ((PromiseAllSettledResultFulfilled<int>)values[1]).Value);
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllSettledGenericEmptyCase()
        {
            var all = Promise.AllSettled<int>(Enumerable.Empty<IPromise>());

            var completed = 0;
            all.Done(values =>
            {
                ++completed;
                Assert.AreEqual(0, values.Count());
            });

            Assert.AreEqual(1, completed);
        }

        public void PromiseAllSettledGenericSingleType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var progress1 = 0f;
            var progress2 = 0f;

            var all1 = Promise.AllSettled<int>(new ISettleablePromise<int>[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var all2 = Promise.AllSettled<int>(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            all1.Done(values =>
            {
                ++callback1;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });
            all2.Done(values =>
            {
                ++callback2;

                Assert.AreEqual(2, values.Length);
                Assert.AreEqual(1, values[0]);
                Assert.AreEqual(2, values[1]);
            });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved(1);

            Assert.AreEqual(0.5f, progress1);
            Assert.AreEqual(0.5f, progress2);

            promise2.ReportResolved(2);

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseAllSettledNonGeneric()
        {
            var promise1 = Promise.Create();
            var promise2 = Promise.Create();

            var progress1 = 0f;
            var progress2 = 0f;

            var all1 = Promise.AllSettled(new ISettleablePromise[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var all2 = Promise.AllSettled(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            all1.Done(() => { ++callback1; });
            all2.Done(() => { ++callback2; });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved();

            Assert.AreEqual(0.5f, progress1);
            Assert.AreEqual(0.5f, progress2);

            promise2.ReportResolved();

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseAllSettledNonGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve();
            var promise2 = Promise.Resolve();

            var all = Promise.AllSettled(promise1, promise2);

            var completed = 0;
            all.Done(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAllSettledNonGenericEmptyCase()
        {
            var all = Promise.AllSettled();

            var completed = 0;
            all.Done(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }
    }
}
