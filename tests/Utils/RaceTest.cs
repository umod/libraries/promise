﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace uMod.Tests.Utils
{
    [TestClass]
    public class RaceTest
    {
        [TestMethod]
        public void PromiseRaceSameType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var race = Promise.Race(promise1, promise2);

            var completed = 0;

            race.Done(value =>
            {
                ++completed;
                Assert.AreEqual(1, value);
            });

            promise2.ReportResolved(1);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseRace2MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();

            var race = Promise.Race<object>(promise1, promise2);

            var completed = 0;

            race.Done(value =>
            {
                ++completed;
                Assert.AreEqual("foo", value);
            });

            promise2.ReportResolved("foo");

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseRace3MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();
            var promise3 = Promise.Create<bool>();

            var race = Promise.Race<object>(promise1, promise2, promise3);

            var completed = 0;

            race.Done(value =>
            {
                ++completed;
                Assert.AreEqual("foo", value);
            });

            promise2.ReportResolved("foo");
            promise1.ReportResolved(1);
            promise3.ReportResolved(true);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseRace8MixedType()
        {
            var promise0 = Promise.Create<int>();
            var promise1 = Promise.Create<string>();
            var promise2 = Promise.Create<bool>();
            var promise3 = Promise.Create<float>();
            var promise4 = Promise.Create<double>();
            var promise5 = Promise.Create<long>();
            var promise6 = Promise.Create<char>();
            var promise7 = Promise.Create();

            var progress = 0f;

            var race = Promise.Race<object>(promise0, promise1, promise2, promise3, promise4, promise5, promise6, promise7)
                .Progress(p => { progress = p; });

            var completed = 0;

            race.Done(value =>
            {
                ++completed;
                Assert.AreEqual(default, value);
            });

            Assert.AreEqual(0f, progress);
            promise7.ReportResolved();
            Assert.AreEqual(1f, progress);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseRaceGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve(1);
            var promise2 = Promise.Create<int>();

            var race = Promise.Race<int>(promise1, promise2);

            var completed = 0;
            race.Done(value =>
            {
                ++completed;
                Assert.AreEqual(1, value);
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseRaceGenericEmptyCase()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                Promise.Race<int>(Enumerable.Empty<IPromise>());
            });
        }

        public void PromiseRaceGenericSingleType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var progress1 = 0f;
            var progress2 = 0f;

            var race1 = Promise.Race<int>(new ISettleablePromise<int>[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var race2 = Promise.Race<int>(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            race1.Done((value) =>
            {
                ++callback1;
                Assert.AreEqual(2, value);
            });
            race2.Done((value) =>
            {
                ++callback2;
                Assert.AreEqual(2, value);
            });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise2.ReportRejected(new Exception());

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseRaceNonGeneric()
        {
            var promise1 = Promise.Create();
            var promise2 = Promise.Create();

            var progress1 = 0f;
            var progress2 = 0f;

            var race1 = Promise.Race(new ISettleablePromise[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var race2 = Promise.Race(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            race1.Done(() => { ++callback1; });
            race2.Done(() => { ++callback2; });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved();

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseRaceNonGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve();
            var promise2 = Promise.Create();

            var race = Promise.Race(promise1, promise2);

            var completed = 0;
            race.Done(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseRaceNonGenericEmptyCase()
        {
            Assert.ThrowsException<ArgumentException>(() =>
            {
                Promise.Race();
            });
        }
    }
}
