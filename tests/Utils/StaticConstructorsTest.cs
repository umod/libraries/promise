﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Utils
{
    [TestClass]
    public class StaticConstructorsTest
    {
        [TestMethod]
        public void PromiseRejectedGeneric()
        {
            Exception ex = new Exception();
            var promise = Promise.Reject<int>(ex);
            var errors = 0;
            promise.Fail((exception) =>
            {
                Assert.AreEqual(ex, exception);
                errors++;
            });

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void PromiseRejectedNonGeneric()
        {
            Exception ex = new Exception();
            var promise = Promise.Reject(ex);
            var errors = 0;
            promise.Fail((exception) =>
            {
                Assert.AreEqual(ex, exception);
                errors++;
            });

            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void PromiseResolvedGeneric()
        {
            var promisedValue = 1;
            var promise = Promise.Resolve(promisedValue);
            var completed = 0;
            promise.Then((value) =>
            {
                Assert.AreEqual(promisedValue, value);
                completed++;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseResolvedNonGeneric()
        {
            var promise = Promise.Resolve();
            var completed = 0;
            promise.Then(() =>
            {
                completed++;
            });

            Assert.AreEqual(1, completed);
        }
    }
}
