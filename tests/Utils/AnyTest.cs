﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace uMod.Tests.Utils
{
    [TestClass]
    public class AnyTest
    {
        [TestMethod]
        public void PromiseAnySameType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var any = Promise.Any(promise1, promise2);

            var completed = 0;

            any.Done(value =>
            {
                ++completed;
                Assert.AreEqual(1, value);
            });

            promise1.ReportRejected(new Exception());
            promise2.ReportResolved(1);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAny2MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();

            var any = Promise.Any<object>(promise1, promise2);

            var completed = 0;

            any.Done(value =>
            {
                ++completed;
                Assert.AreEqual("foo", value);
            });

            promise1.ReportRejected(new Exception());
            promise2.ReportResolved("foo");

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAny3MixedType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<string>();
            var promise3 = Promise.Create<bool>();

            var any = Promise.Any<object>(promise1, promise2, promise3);

            var completed = 0;

            any.Done(value =>
            {
                ++completed;
                Assert.AreEqual("foo", value);
            });

            promise1.ReportRejected(new Exception());
            promise2.ReportResolved("foo");
            promise3.ReportResolved(true);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAny8MixedType()
        {
            var promise0 = Promise.Create<int>();
            var promise1 = Promise.Create<string>();
            var promise2 = Promise.Create<bool>();
            var promise3 = Promise.Create<float>();
            var promise4 = Promise.Create<double>();
            var promise5 = Promise.Create<long>();
            var promise6 = Promise.Create<char>();
            var promise7 = Promise.Create();

            var progress = 0f;

            var any = Promise.Any<object>(promise0, promise1, promise2, promise3, promise4, promise5, promise6, promise7)
                .Progress(p => { progress = p; });

            var completed = 0;

            any.Done(value =>
            {
                ++completed;
                Assert.AreEqual(default, value);
            });

            Assert.AreEqual(0f, progress);
            promise1.ReportRejected(new Exception());
            Assert.AreEqual(0f, progress);
            promise5.ReportRejected(new Exception());
            Assert.AreEqual(0f, progress);
            promise7.ReportResolved();
            Assert.AreEqual(1f, progress);

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAnyGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve(1);
            var promise2 = Promise.Create<int>();

            var any = Promise.Any<int>(promise1, promise2);

            var completed = 0;
            any.Done(value =>
            {
                ++completed;
                Assert.AreEqual(1, value);
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAnyGenericEmptyCase()
        {
            var any = Promise.Any<int>(Enumerable.Empty<IPromise>());

            var errors = 0;
            any.Fail((exception) =>
            {
                ++errors;
            });

            Assert.AreEqual(1, errors);
        }

        public void PromiseAnyGenericSingleType()
        {
            var promise1 = Promise.Create<int>();
            var promise2 = Promise.Create<int>();

            var progress1 = 0f;
            var progress2 = 0f;

            var any1 = Promise.Any<int>(new ISettleablePromise<int>[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var any2 = Promise.Any<int>(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            any1.Done((value) =>
            {
                ++callback1;
                Assert.AreEqual(1, value);
            });
            any2.Done((value) =>
            {
                ++callback2;
                Assert.AreEqual(1, value);
            });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved(1);

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseAnyNonGeneric()
        {
            var promise1 = Promise.Create();
            var promise2 = Promise.Create();

            var progress1 = 0f;
            var progress2 = 0f;

            var any1 = Promise.Any(new ISettleablePromise[] { promise1, promise2 }).Progress(p => { progress1 = p; });
            var any2 = Promise.Any(promise1, promise2).Progress(p => { progress2 = p; });

            var callback1 = 0;
            var callback2 = 0;

            any1.Done(() => { ++callback1; });
            any2.Done(() => { ++callback2; });

            Assert.AreEqual(0f, progress1);
            Assert.AreEqual(0f, progress2);

            promise1.ReportResolved();

            Assert.AreEqual(1f, progress1);
            Assert.AreEqual(1f, progress2);

            Assert.AreEqual(1, callback1);
            Assert.AreEqual(1, callback2);
        }

        [TestMethod]
        public void PromiseAnyNonGenericAlreadyResolved()
        {
            var promise1 = Promise.Resolve();
            var promise2 = Promise.Create();

            var any = Promise.Any(promise1, promise2);

            var completed = 0;
            any.Done(() =>
            {
                ++completed;
            });

            Assert.AreEqual(1, completed);
        }

        [TestMethod]
        public void PromiseAnyNonGenericEmptyCase()
        {
            var any = Promise.Any();

            var errors = 0;
            any.Fail((exception) =>
            {
                ++errors;
            });

            Assert.AreEqual(1, errors);
        }
    }
}
