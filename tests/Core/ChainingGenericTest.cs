﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Core
{
    [TestClass]
    public class ChainingGenericTest
    {
        [TestMethod]
        public void ResolvePromiseChainFinallyCallback()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise
                .Finally(() =>
                {
                    Assert.AreEqual(1, ++callbacks);
                })
                .Catch((Exception exception) =>
                {
                    callbacks++;
                    Assert.Fail();
                })
                .Done(() =>
                {
                    Assert.AreEqual(2, ++callbacks);
                });

            promise.ReportResolved(1);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void ResolvePromiseChainGenericFinallyCallback()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise
                .Finally(() =>
                {
                    Assert.AreEqual(1, ++callbacks);
                    return Promise.Resolve(1);
                })
                .Then((value) =>
                {
                    Assert.AreEqual(2, ++callbacks);
                    Assert.AreEqual(1, value);
                },
                (Exception exception) =>
                {
                    callbacks++;
                    Assert.Fail();
                })
                .Done();

            promise.ReportResolved(1);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void ResolvePromiseChainCallbackNullReturn()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise
                .Then(() =>
                {
                    Assert.AreEqual(1, ++callbacks);
                    return null;
                })
                .Done(() =>
                {
                    Assert.AreEqual(2, ++callbacks);
                });

            promise.ReportResolved(1);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void ResolvePromiseChainCallbackWithNonGenericPromise()
        {
            var promise = Promise.Create<int>();
            var resultPromise = Promise.Create();

            var callbacks = 0;

            promise
                .Then(value =>
                {
                    Assert.AreEqual(1, value);
                    return (IPromise)resultPromise;
                })
                .Done(() =>
                {
                    Assert.AreEqual(1, ++callbacks);
                });

            promise.ReportResolved(1);
            resultPromise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void ResolvePromiseThenCallbacks()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) =>
                {
                    Assert.AreEqual(1, ++callbacks);
                    Assert.AreEqual(1, value);
                })
                .Done();
            promise.Then(
                (value) =>
                {
                    Assert.AreEqual(2, ++callbacks);
                    Assert.AreEqual(1, value);
                })
                .Done();

            promise.ReportResolved(1);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void ResolvePromiseThenPromiseCallback()
        {
            var nestedPromise = Promise.Create<int>();
            var callbacks = 0;

            nestedPromise.Done((value) =>
            {
                Assert.AreEqual(1, ++callbacks);
                Assert.AreEqual(1, value);
            });

            var promise = Promise.Resolve(nestedPromise);

            promise.Done((value) =>
            {
                Assert.AreEqual(2, ++callbacks);
                Assert.AreEqual(1, value);
            });

            nestedPromise.ReportResolved(1);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void RejectPromiseCatchCallbacks()
        {
            var promise = Promise.Create<int>();
            var ex = new Exception();
            var callbacks = 0;

            promise.Catch(
                (Exception exception) =>
                {
                    Assert.AreEqual(ex, exception);
                    Assert.AreEqual(1, ++callbacks);
                })
                .Done();
            promise.Catch((Exception exception) =>
                {
                    Assert.AreEqual(ex, exception);
                    Assert.AreEqual(2, ++callbacks);
                })
                .Done();

            promise.ReportRejected(ex);

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void RejectPromiseChainFinallyCallback()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise
                .Finally(() =>
                {
                    Assert.AreEqual(1, ++callbacks);
                })
                .Catch((Exception exception) =>
                {
                    callbacks++;
                    Assert.Fail();
                })
                .Then(() =>
                {
                    Assert.AreEqual(2, ++callbacks);
                })
                .Done();

            promise.ReportRejected(new Exception());

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void RejectPromiseChainGenericFinallyCallback()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise
                .Finally(() =>
                {
                    Assert.AreEqual(1, ++callbacks);
                    return Promise.Resolve(1);
                })
                .Done((value) =>
                {
                    Assert.AreEqual(2, ++callbacks);
                    Assert.AreEqual(1, value);
                },
                (Exception exception) =>
                {
                    callbacks++;
                    Assert.Fail();
                });

            promise.ReportRejected(new Exception());

            Assert.AreEqual(2, callbacks);
        }

        [TestMethod]
        public void RejectPromiseNestedCatch()
        {
            var promise = Promise.Create<int>();
            var ex = new Exception();
            bool works = false;
            promise
                .Catch((Exception exception) => Promise.Reject(exception))
                .Then(() => { Assert.Fail(); })
                .Done(null, (Exception exception) => { works = ex == exception; });

            promise.ReportRejected(ex);

            Assert.IsTrue(works);
        }
    }
}
