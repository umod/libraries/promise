﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Core
{
    [TestClass]
    public class StateExceptionsTest
    {
        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectRejectGeneric()
        {
            var promise = Promise.Create<int>();
            promise.ReportRejected(new Exception());
            promise.ReportRejected(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectRejectNonGeneric()
        {
            var promise = Promise.Create();
            promise.ReportRejected(new Exception());
            promise.ReportRejected(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectResolveGeneric()
        {
            var promise = Promise.Create<int>();
            promise.ReportResolved(5);
            promise.ReportRejected(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterRejectResolveNonGeneric()
        {
            var promise = Promise.Create();
            promise.ReportResolved();
            promise.ReportRejected(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveRejectGeneric()
        {
            var promise = Promise.Create<int>();
            promise.ReportRejected(new Exception());
            promise.ReportResolved(5);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveRejectNonGeneric()
        {
            var promise = Promise.Create();
            promise.ReportRejected(new Exception());
            promise.ReportResolved();
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveResolveGeneric()
        {
            var promise = Promise.Create<int>();
            promise.ReportResolved(5);
            promise.ReportResolved(5);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseStateException))]
        public void ExceptionAfterResolveResolveNonGeneric()
        {
            var promise = Promise.Create();
            promise.ReportResolved();
            promise.ReportResolved();
        }
    }
}
