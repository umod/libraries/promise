﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace uMod.Tests.Core
{
    [TestClass]
    public class CovarianceAndContravariance
    {
        [TestMethod]
        public void PromiseCovariance()
        {
            IPromise<object> promise = Promise.Resolve("string");
            Assert.IsNotNull(promise);
        }

        [TestMethod]
        public void PromiseContravariance()
        {
            ISettleable<string> promise = Promise.Create<object>();
            Assert.IsNotNull(promise);
        }
    }
}
