﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Core
{
    [TestClass]
    public class UnhandledExceptionsTest
    {
        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledRejectedPromiseNonGeneric()
        {
            var promise = Promise.Create();

            promise
                .Then(() => throw new Exception())
                .Done();

            promise.ReportResolved();
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledRejectedPromiseNonGeneric_Presettled()
        {
            var promise = Promise.Resolve();

            promise
                .Then(() => throw new Exception())
                .Done();
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneFulfilledHandlerNonGeneric()
        {
            var promise = Promise.Create();
            promise.Done(() => { throw new Exception(); });
            promise.ReportResolved();
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneFulfilledHandlerNonGeneric_Presettled()
        {
            var promise = Promise.Resolve();
            promise.Done(() => { throw new Exception(); });
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneRejectedHandlerNonGeneric()
        {
            var promise = Promise.Create();
            promise.Done(null, (Exception e) => { throw new Exception(); });
            promise.ReportRejected(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneRejectedHandlerNonGeneric_Presettled()
        {
            var promise = Promise.Reject<int>(new Exception());
            promise.Done(null, (Exception e) => { throw new Exception(); });
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledRejectedPromiseGeneric()
        {
            var promise = Promise.Create<int>();

            promise
                .Then(() => throw new Exception())
                .Done();

            promise.ReportResolved(1);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledRejectedPromiseGeneric_Presettled()
        {
            var promise = Promise.Resolve(1);

            promise
                .Then(() => throw new Exception())
                .Done();
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneFulfilledHandlerGeneric()
        {
            var promise = Promise.Create<int>();
            promise.Done(() => { throw new Exception(); });
            promise.ReportResolved(1);
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneFulfilledHandlerGeneric_Presettled()
        {
            var promise = Promise.Resolve(1);
            promise.Done(() => { throw new Exception(); });
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneRejectedHandlerGeneric()
        {
            var promise = Promise.Create<int>();
            promise.Done(null, (Exception e) => { throw new Exception(); });
            promise.ReportRejected(new Exception());
        }

        [TestMethod]
        [ExpectedException(typeof(PromiseUnhandledException))]
        public void UnhandledInDoneRejectedHandlerGeneric_Presettled()
        {
            var promise = Promise.Reject<int>(new Exception());
            promise.Done(null, (Exception e) => { throw new Exception(); });
        }
    }
}
