﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Core
{
    [TestClass]
    public class BasicNonGenericTest
    {
        [TestMethod]
        public void Then1()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(() => { ++callbacks; });
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then1_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(() => { ++callbacks; });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then2()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(() => { ++callbacks; return Promise.Resolve(); });
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then2_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(() => { ++callbacks; return Promise.Resolve(); });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then3()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => throw ex
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then3_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => throw ex
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then4()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => throw ex
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then4_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => throw ex
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then5()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => Promise.Reject(ex)
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then5_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => Promise.Reject(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then6()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => Promise.Reject(ex)
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then6_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => Promise.Reject(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then7()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(() => ++callbacks);
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then7_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(() => ++callbacks);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then8()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(() => Promise.Resolve(++callbacks));
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then8_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(() => Promise.Resolve(++callbacks));

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then9()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (PromiseOnReject<int>)((ex) => throw ex)
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then9_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (PromiseOnReject<int>)((ex) => throw ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then10()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (ex) => Promise.Reject<int>(ex)
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then10_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (ex) => Promise.Reject<int>(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then11()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (PromiseOnReject<int>)((ex) => throw ex)
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then11_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (PromiseOnReject<int>)((ex) => throw ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then12()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (ex) => Promise.Reject<int>(ex)
            );
            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then12_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (ex) => Promise.Reject<int>(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch1()
        {
            var promise = Promise.Create();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch1_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch2()
        {
            var promise = Promise.Create();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve();
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch2_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve();
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch3()
        {
            var promise = Promise.Create();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return 5;
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch3_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return 5;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch4()
        {
            var promise = Promise.Create();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve(5);
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch4_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve(5);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally1()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
            });

            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally1_Presettled()
        {
            var promise = Promise.Reject(new Exception());
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally2()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve();
            });

            promise.ReportRejected(new Exception());

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally2_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve();
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally3()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return 5;
            });

            promise.ReportResolved();

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally3_Presettled()
        {
            var promise = Promise.Reject(new Exception());
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return 5;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally4()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve(5);
            });

            promise.ReportRejected(new Exception());

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally4_Presettled()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve(5);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Progress()
        {
            var promise = Promise.Create();
            var callbacks = 0;

            promise.Progress((progress) =>
            {
                callbacks++;
            });

            promise.ReportProgress(0.1f);
            promise.ReportProgress(0.3f);
            promise.ReportProgress(0.5f);
            promise.ReportProgress(0.7f);
            promise.ReportResolved();

            Assert.AreEqual(5, callbacks);
        }

        [TestMethod]
        public void FulfillsViaResolver1()
        {
            var promise = Promise.Create((resolve, reject) => resolve());
            var callbacks = 0;
            promise.Then(() =>
            {
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void FulfillsViaResolver2()
        {
            var promise = Promise.Create((resolve, reject, progress) => resolve());
            var callbacks = 0;
            promise.Then(() =>
            {
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolver1()
        {
            Exception ex = new Exception();
            var promise = Promise.Create((resolve, reject) => { reject(ex); });
            var callbacks = 0;
            promise.Catch(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolver2()
        {
            Exception ex = new Exception();
            var promise = Promise.Create((resolve, reject, progress) => { reject(ex); });
            var callbacks = 0;
            promise.Catch(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolverException1()
        {
            Exception ex = new Exception();
            var promise = Promise.Create((resolve, reject) => throw ex);
            var callbacks = 0;
            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolverException2()
        {
            Exception ex = new Exception();
            var promise = Promise.Create((resolve, reject, progress) => throw ex);
            var callbacks = 0;
            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void NewResolved()
        {
            var promise = Promise.Resolve();
            var callbacks = 0;
            promise.Done(() =>
            {
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void NewRejected()
        {
            Exception ex = new Exception();
            var promise = Promise.Reject(ex);
            var callbacks = 0;
            promise.Fail((exception) =>
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }
    }
}
