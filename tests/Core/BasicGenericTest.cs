﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Core
{
    [TestClass]
    public class BasicGenericTest
    {
        [TestMethod]
        public void Then1()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(() => { ++callbacks; });
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then1_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(() => { ++callbacks; });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then2()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(() => { ++callbacks; return Promise.Resolve(); });
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then2_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(() => { ++callbacks; return Promise.Resolve(); });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then3()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => throw ex
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then3_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => throw ex
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then4()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => throw ex
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then4_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => throw ex
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then5()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => Promise.Reject(ex)
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then5_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; },
                (ex) => Promise.Reject(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then6()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => Promise.Reject(ex)
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then6_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => { ++callbacks; return Promise.Resolve(); },
                (ex) => Promise.Reject(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then7()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(() => ++callbacks);
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then7_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(() => ++callbacks);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then8()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(() => Promise.Resolve(++callbacks));
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then8_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(() => Promise.Resolve(++callbacks));

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then9()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (PromiseOnReject<int>)((ex) => throw ex)
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then9_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (PromiseOnReject<int>)((ex) => throw ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then10()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (ex) => Promise.Reject<int>(ex)
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then10_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => ++callbacks,
                (ex) => Promise.Reject<int>(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then11()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (PromiseOnReject<int>)((ex) => throw ex)
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then11_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (PromiseOnReject<int>)((ex) => throw ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then12()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (ex) => Promise.Reject<int>(ex)
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then12_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                () => Promise.Resolve(++callbacks),
                (ex) => Promise.Reject<int>(ex)
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then13()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then((value) => { ++callbacks; });
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then13_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then((value) => { ++callbacks; });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then14()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then((value) => { ++callbacks; return Promise.Resolve(); });
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then14_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then((value) => { ++callbacks; return Promise.Resolve(); });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then15()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then((value) => ++callbacks);
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then15_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then((value) => ++callbacks);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then16()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then((value) => { ++callbacks; return Promise.Resolve(1); });
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then16_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then((value) => { ++callbacks; return Promise.Resolve(1); });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then17()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; },
                (exception) => { }
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then17_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; },
                (exception) => { }
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then18()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(); },
                (exception) => { }
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then18_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(); },
                (exception) => { }
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then19()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; },
                (exception) => Promise.Resolve()
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then19_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; },
                (exception) => Promise.Resolve()
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then20()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(); },
                (exception) => Promise.Resolve()
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then20_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(); },
                (exception) => Promise.Resolve()
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then21()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => ++callbacks,
                (exception) => ++callbacks
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then21_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => ++callbacks,
                (exception) => ++callbacks
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then22()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => ++callbacks,
                (exception) => { ++callbacks; return Promise.Resolve(1); }
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then22_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => ++callbacks,
                (exception) => { ++callbacks; return Promise.Resolve(1); }
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then23()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(1); },
                (exception) => ++callbacks
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then23_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(1); },
                (exception) => ++callbacks
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then24()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(1); },
                (exception) => { ++callbacks; return Promise.Resolve(1); }
            );
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Then24_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Then(
                (value) => { ++callbacks; return Promise.Resolve(1); },
                (exception) => { ++callbacks; return Promise.Resolve(1); }
            );

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Done1()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Done(() => { ++callbacks; });
            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Done1_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Done(() => { ++callbacks; });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Done2()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;
            var errors = 0;

            promise.Done(() => { ++callbacks; }, (exception) => { ++errors; });
            promise.ReportRejected(new Exception());

            Assert.AreEqual(0, callbacks);
            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void Done2_Presettled()
        {
            var promise = Promise.Reject<int>(new Exception());
            var callbacks = 0;
            var errors = 0;

            promise.Done(() => { ++callbacks; }, (exception) => { ++errors; });

            Assert.AreEqual(0, callbacks);
            Assert.AreEqual(1, errors);
        }

        [TestMethod]
        public void Catch1()
        {
            var promise = Promise.Create<int>();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch1_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch2()
        {
            var promise = Promise.Create<int>();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve();
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch2_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve();
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch3()
        {
            var promise = Promise.Create<int>();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return 5;
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch3_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return 5;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch4()
        {
            var promise = Promise.Create<int>();
            var reason = new Exception();
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve(5);
            });

            promise.ReportRejected(reason);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Catch4_Presettled()
        {
            var reason = new Exception();
            var promise = Promise.Reject(reason);
            var callbacks = 0;

            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(reason, exception);
                callbacks++;
                return Promise.Resolve(5);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally1()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
            });

            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally1_Presettled()
        {
            var promise = Promise.Reject(new Exception());
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally2()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve();
            });

            promise.ReportRejected(new Exception());

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally2_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve();
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally3()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return 5;
            });

            promise.ReportResolved(1);

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally3_Presettled()
        {
            var promise = Promise.Reject(new Exception());
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return 5;
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Finally4()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve(5);
            });

            promise.ReportRejected(new Exception());

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void Progress()
        {
            var promise = Promise.Create<int>();
            var callbacks = 0;

            promise.Progress((progress) =>
            {
                callbacks++;
            });

            promise.ReportProgress(0.1f);
            promise.ReportProgress(0.3f);
            promise.ReportProgress(0.5f);
            promise.ReportProgress(0.7f);
            promise.ReportResolved(1);

            Assert.AreEqual(5, callbacks);
        }

        [TestMethod]
        public void Finally4_Presettled()
        {
            var promise = Promise.Resolve(1);
            var callbacks = 0;

            promise.Finally(() =>
            {
                callbacks++;
                return Promise.Resolve(5);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void FulfillsViaResolver1()
        {
            var promise = Promise.Create<int>((resolve, reject) => resolve(1));
            var callbacks = 0;
            promise.Then((value) =>
            {
                Assert.AreEqual(1, ++callbacks);
                Assert.AreEqual(1, value);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void FulfillsViaResolver2()
        {
            var promise = Promise.Create<int>((resolve, reject, progress) => resolve(1));
            var callbacks = 0;
            promise.Then((value) =>
            {
                Assert.AreEqual(1, ++callbacks);
                Assert.AreEqual(1, value);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolver1()
        {
            Exception ex = new Exception();
            var promise = Promise.Create<int>((resolve, reject) => { reject(ex); });
            var callbacks = 0;
            promise.Catch(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolver2()
        {
            Exception ex = new Exception();
            var promise = Promise.Create<int>((resolve, reject, progress) => { reject(ex); });
            var callbacks = 0;
            promise.Catch(delegate (Exception exception)
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolverException1()
        {
            Exception ex = new Exception();
            var promise = Promise.Create<int>((resolve, reject) => throw ex);
            var callbacks = 0;
            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void RejectsViaResolverException2()
        {
            Exception ex = new Exception();
            var promise = Promise.Create<int>((resolve, reject, progress) => throw ex);
            var callbacks = 0;
            promise.Catch((Exception exception) =>
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void NewResolved()
        {
            var val = 1;
            var promise = Promise.Resolve(val);
            var callbacks = 0;
            promise.Done((value) =>
            {
                Assert.AreEqual(val, value);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }

        [TestMethod]
        public void NewRejected()
        {
            Exception ex = new Exception();
            var promise = Promise.Reject<int>(ex);
            var callbacks = 0;
            promise.Fail((exception) =>
            {
                Assert.AreEqual(ex, exception);
                Assert.AreEqual(1, ++callbacks);
            });

            Assert.AreEqual(1, callbacks);
        }
    }
}
