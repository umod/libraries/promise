﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Performance
{
    [TestClass]
    public class PeekChainTest
    {
        [TestMethod]
        public void PeekThen1()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a
                .ThenPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            b.Done();

            a.ReportResolved();

            Assert.AreEqual(2, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekThen1_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a
                .ThenPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            b.Done();

            Assert.AreEqual(2, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekThen2()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a
                .ThenPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            var c = b.Then(() => { callbacks++; });
            c.Done();

            a.ReportResolved();

            Assert.AreEqual(3, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekThen2_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a
                .ThenPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            var c = b.Then(() => { callbacks++; });
            c.Done();

            Assert.AreEqual(3, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekThen3()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b
                .ThenPeek(() => { callbacks++; })
                .Catch((exception) => { });
            c.Done();

            a.ReportResolved();

            Assert.AreEqual(0, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekThen3_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b
                .ThenPeek(() => { callbacks++; })
                .Catch((exception) => { });
            c.Done();

            Assert.AreEqual(0, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekCatch1()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a
                .CatchPeek((ex) => { callbacks++; })
                .Then(() => { callbacks++; });
            b.Done();

            a.ReportResolved();

            Assert.AreEqual(1, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekCatch1_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a
                .CatchPeek((ex) => { callbacks++; })
                .Then(() => { callbacks++; });
            b.Done();

            Assert.AreEqual(1, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekCatch2()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a
                .CatchPeek((ex) => { callbacks++; })
                .Then(() => { callbacks++; });
            var c = b.Then(() => { callbacks++; });
            c.Done();

            a.ReportResolved();

            Assert.AreEqual(2, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekCatch2_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a
                .CatchPeek((ex) => { callbacks++; })
                .Then(() => { callbacks++; });
            var c = b.Then(() => { callbacks++; });
            c.Done();

            Assert.AreEqual(2, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekCatch3()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b
                .CatchPeek((ex) => { callbacks++; })
                .Catch((exception) => { });
            c.Done();

            a.ReportResolved();

            Assert.AreEqual(1, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekCatch3_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b
                .CatchPeek((ex) => { callbacks++; })
                .Catch((exception) => { });
            c.Done();

            Assert.AreEqual(1, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekFinally1()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a
                .FinallyPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            b.Done();

            a.ReportResolved();

            Assert.AreEqual(2, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekFinally1_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a
                .FinallyPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            b.Done();

            Assert.AreEqual(2, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekFinally2()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a
                .FinallyPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            var c = b.Then(() => { callbacks++; });
            c.Done();

            a.ReportResolved();

            Assert.AreEqual(3, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekFinally2_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a
                .FinallyPeek(() => { callbacks++; })
                .Then(() => { callbacks++; });
            var c = b.Then(() => { callbacks++; });
            c.Done();

            Assert.AreEqual(3, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekFinally3()
        {
            var callbacks = 0;

            var a = Promise.Create();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b
                .FinallyPeek(() => { callbacks++; })
                .Catch((exception) => { });
            c.Done();

            a.ReportResolved();

            Assert.AreEqual(1, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void PeekFinally3_Presettled()
        {
            var callbacks = 0;

            var a = Promise.Resolve();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b
                .FinallyPeek(() => { callbacks++; })
                .Catch((exception) => { });
            c.Done();

            Assert.AreEqual(1, callbacks);
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }
    }
}
