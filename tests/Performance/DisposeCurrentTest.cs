﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Performance
{
    [TestClass]
    public class DisposeCurrentTest
    {
        [TestMethod]
        public void DisposeOnDone()
        {
            var promise = Promise.Create();
            promise.Done();
            promise.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                promise.ThenPeek(() => { });
            });
        }

        [TestMethod]
        public void DisposeOnDone_Presettled()
        {
            var promise = Promise.Resolve();
            promise.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                promise.ThenPeek(() => { });
            });
        }
    }
}
