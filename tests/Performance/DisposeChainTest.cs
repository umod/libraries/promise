﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace uMod.Tests.Performance
{
    [TestClass]
    public class DisposeChainTest
    {
        [TestMethod]
        public void DisposeChainOnDone1()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            b.Done();

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDone1_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            b.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDone2()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            var c = b.Then(() => { });
            c.Done();

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDone2_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            var c = b.Then(() => { });
            c.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDone3()
        {
            var a = Promise.Create();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b.Catch((exception) => { });
            c.Done();

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDone3_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b.Catch((exception) => { });
            c.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDoneFork1()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            var c = a.Then(() => { });
            b.Done();

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });

            a.ThenPeek(() => {});
            c.ThenPeek(() => {});
            c.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnDoneFork1_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            var c = a.Then(() => { });
            b.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });

            a.ThenPeek(() => {});
            c.ThenPeek(() => {});
            c.Done();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFail1()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            b.Fail((excption) => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFail1_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            b.Fail((excption) => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFail2()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            var c = b.Then(() => { });
            c.Fail((excption) => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFail2_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            var c = b.Then(() => { });
            c.Fail((excption) => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFail3()
        {
            var a = Promise.Create();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b.Catch((exception) => { });
            c.Fail((excption) => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFail3_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b.Catch((exception) => { });
            c.Fail((excption) => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFailFork1()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            var c = a.Then(() => { });
            b.Fail((excption) => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });

            a.ThenPeek(() => {});
            c.ThenPeek(() => {});
            c.Fail((excption) => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnFailFork1_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            var c = a.Then(() => { });
            b.Fail((excption) => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });

            a.ThenPeek(() => {});
            c.ThenPeek(() => {});
            c.Fail((excption) => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnComplete1()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            b.Complete(() => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnComplete1_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            b.Complete(() => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnComplete2()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            var c = b.Then(() => { });
            c.Complete(() => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnComplete2_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            var c = b.Then(() => { });
            c.Complete(() => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnComplete3()
        {
            var a = Promise.Create();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b.Catch((exception) => { });
            c.Complete(() => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnComplete3_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => Promise.Reject(new Exception()));
            var c = b.Catch((exception) => { });
            c.Complete(() => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnCompleteFork1()
        {
            var a = Promise.Create();
            var b = a.Then(() => { });
            var c = a.Then(() => { });
            b.Complete(() => { });

            a.ReportResolved();

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });

            a.ThenPeek(() => {});
            c.ThenPeek(() => {});
            c.Complete(() => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }

        [TestMethod]
        public void DisposeChainOnCompleteFork1_Presettled()
        {
            var a = Promise.Resolve();
            var b = a.Then(() => { });
            var c = a.Then(() => { });
            b.Complete(() => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                b.ThenPeek(() => {});
            });

            a.ThenPeek(() => {});
            c.ThenPeek(() => {});
            c.Complete(() => { });

            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                c.ThenPeek(() => {});
            });
            Assert.ThrowsException<ObjectDisposedException>(() =>
            {
                a.ThenPeek(() => {});
            });
        }
    }
}
